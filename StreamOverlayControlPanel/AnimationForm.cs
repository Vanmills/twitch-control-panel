﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StreamOverlayControlPanel
{
    public partial class AnimationForm : Form
    {
        private AnimationSettings settings;
        private UdpClient client;
        public AnimationForm()
        {
            InitializeComponent();
        }

        public AnimationForm(AnimationSettings pSettings, UdpClient pClient)
        {
            InitializeComponent();
            settings = pSettings;
            client = pClient;
            SetSettings();
        }

        public void SetSettings()
        {

            animateFileDialog.FileName = settings.filenameGraphic;

            zoomOutCheckbox.Checked = settings.zoomOut;
            totalAnimationDurationTextbox.Text = settings.totalDuration.ToString();
            
            animFadeInCheckbox.Checked = settings.fadeIn;
            fadeInDurationTextbox.Text = settings.fadeInDuration.ToString();

            animFadeOutCheckbox.Checked = settings.fadeOut;
            fadeOutDurationTextbox.Text = settings.fadeOutDuration.ToString();
            fadeOutTimeTextbox.Text = settings.fadeOutStartTime.ToString();

            rotateInCheckbox.Checked = settings.rotateIn;
            rotateInClockwiseCheckbox.Checked = !settings.rotateInClockwise;
            rotateInStartTextbox.Text = settings.rotateInStartTime.ToString();
            rotateInDurationTextbox.Text = settings.rotateInDuration.ToString();
            rotateInDegreesTextbox.Text = settings.rotationInDegrees.ToString();

            rotateOutCheckbox.Checked= settings.rotateOut;
            rotateOutClocwiseCheckbox.Checked = !settings.rotateOutClockwise;
            rotateOutStartTextbox.Text = settings.rotateOutStartTime.ToString();
            rotateOutDegreesTextbox.Text = settings.rotationOutDegrees.ToString();
            rotateOutDurationTextbox.Text = settings.rotateOutDuration.ToString();

            zoomInCheckbox.Checked = settings.zoomIn;
            zoomInSizeTextbox.Text = settings.zoomInSize.ToString();
            zoomInSpeedTextbox.Text = settings.zoomInSpeed.ToString();
            zoomInStartTimeTextbox.Text = settings.zoomInStartTime.ToString();

            zoomOutSizeTextbox.Text = settings.zoomOutSize.ToString();
            zoomOutSpeedTextbox.Text = settings.zoomOutSpeed.ToString();
            zoomOutStartTimeTextbox.Text = settings.zoomOutStartTime.ToString();
            zoomOutSizeTextbox.Text = settings.zoomOutSize.ToString();
        }
        
        private void testAnimationButton_Click(object sender, EventArgs e)
        {
            UpdateSettings();


            PipePackage package = new PipePackage();
            package.CreateBuffer(AnimationSettings.SerializeClass(settings), PipePackage.Packagetype.Animation);
            client.Send(package.buffer, package.buffer.Length);
        }

        private void selectFileToAnimate_Click(object sender, EventArgs e)
        {
            DialogResult result = animateFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                settings.filenameGraphic = animateFileDialog.FileName;
                fileNameLabel.Text = settings.filenameGraphic;
            }
        }

        private void UpdateSettings()
        {
            settings.fadeIn = animFadeInCheckbox.Checked;
            settings.fadeOut = animFadeOutCheckbox.Checked;
            settings.rotateIn = rotateInCheckbox.Checked;
            settings.rotateOut = rotateOutCheckbox.Checked;
            settings.zoomIn = zoomInCheckbox.Checked;
            settings.zoomOut = zoomOutCheckbox.Checked;
            settings.rotateInClockwise = !rotateInClockwiseCheckbox.Checked;
            settings.rotateOutClockwise = !rotateOutClocwiseCheckbox.Checked;

            if (!string.IsNullOrEmpty(fadeInDurationTextbox.Text))
            {
                settings.fadeInDuration = float.Parse(fadeInDurationTextbox.Text);
            }
            settings.fadeInStartTime = 0.0f;

            if (!string.IsNullOrEmpty(fadeOutTimeTextbox.Text))
            {
                settings.fadeOutStartTime = float.Parse(fadeOutTimeTextbox.Text);
            }

            if (!string.IsNullOrEmpty(fadeOutDurationTextbox.Text))
            {
                settings.fadeOutDuration = float.Parse(fadeOutDurationTextbox.Text);
            }

            if (!string.IsNullOrEmpty(rotateInStartTextbox.Text))
            {
                settings.rotateInStartTime = float.Parse(rotateInStartTextbox.Text);
            }

            if (!string.IsNullOrEmpty(rotateInDegreesTextbox.Text))
            {
                settings.rotationInDegrees = float.Parse(rotateInDegreesTextbox.Text);
            }
            if (!string.IsNullOrEmpty(rotateInDurationTextbox.Text))
            {
                settings.rotateInDuration = float.Parse(rotateInDurationTextbox.Text);
            }

            if (!string.IsNullOrEmpty(rotateOutStartTextbox.Text))
            {
                settings.rotateOutStartTime = float.Parse(rotateOutStartTextbox.Text);
            }

            if (!string.IsNullOrEmpty(rotateOutDegreesTextbox.Text))
            {
                settings.rotationOutDegrees = float.Parse(rotateOutDegreesTextbox.Text);
            }

            if (!string.IsNullOrEmpty(rotateInDurationTextbox.Text))
            {
                settings.rotateInDuration = float.Parse(rotateInDurationTextbox.Text);
            }

            if (!string.IsNullOrEmpty(zoomInSizeTextbox.Text))
            {
                settings.zoomInSize = float.Parse(zoomInSizeTextbox.Text);
            }

            if (!string.IsNullOrEmpty(zoomInSpeedTextbox.Text))
            {
                settings.zoomInSpeed = float.Parse(zoomInSpeedTextbox.Text);
            }
            if (!string.IsNullOrEmpty(zoomInStartTimeTextbox.Text))
            {
                settings.zoomInStartTime = float.Parse(zoomInStartTimeTextbox.Text);
            }
            if (!string.IsNullOrEmpty(zoomOutSizeTextbox.Text))
            {
                settings.zoomOutSize = float.Parse(zoomOutSizeTextbox.Text);

            }
            if (!string.IsNullOrEmpty(zoomOutSpeedTextbox.Text))
            {
                settings.zoomOutSpeed = float.Parse(zoomOutSpeedTextbox.Text);
            }

            if (!string.IsNullOrEmpty(zoomOutStartTimeTextbox.Text))
            {
                settings.zoomOutStartTime = float.Parse(zoomOutStartTimeTextbox.Text);
            }

            if (string.IsNullOrEmpty(animateFileDialog.FileName))
            {
                settings.filenameGraphic = "";
            }
            else
            {
                settings.filenameGraphic = animateFileDialog.FileName;
            }

        }

        private void saveAndCloseButton_Click(object sender, EventArgs e)
        {
            UpdateSettings();
            if (!string.IsNullOrEmpty(settings.filenameGraphic))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }


        }

        private void validateData(object sender, KeyPressEventArgs e)
        {
            TextBox textBox = (TextBox) sender;
            //fileNameLabel.Text = textBox.Name;

            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar.Equals(',') && e.KeyChar.Equals('.');
            if (!char.IsControl(e.KeyChar) && 
                !char.IsDigit(e.KeyChar)   && 
                e.KeyChar != '.'&& e.KeyChar != ',')
            {
                
                e.Handled = true;
            }

            //check if '.' , ',' pressed
            char sepratorChar = ',';
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
                    if (e.KeyChar == ',')
            {
                // check if it's in the beginning of text not accept
                if (textBox.Text.Length == 0) e.Handled = true;
                // check if it's in the beginning of text not accept
                if (textBox.SelectionStart == 0) e.Handled = true;
                // check if there is already exist a '.' , ','
                if (alreadyExist(textBox.Text, ref sepratorChar)) e.Handled = true;
                //check if '.' or ',' is in middle of a number and after it is not a number greater than 99
                if (textBox.SelectionStart != textBox.Text.Length && e.Handled == false)
                {
                    // '.' or ',' is in the middle
                    string AfterDotString = textBox.Text.Substring(textBox.SelectionStart);

                    if (AfterDotString.Length > 2)
                    {
                        e.Handled = true;
                    }
                }
            }
            //check if a number pressed

            if (Char.IsDigit(e.KeyChar))
            {
                //check if a coma or dot exist
                if (alreadyExist(textBox.Text, ref sepratorChar))
                {
                    int sepratorPosition = textBox.Text.IndexOf(sepratorChar);
                    string afterSepratorString = textBox.Text.Substring(sepratorPosition + 1);
                    if (textBox.SelectionStart > sepratorPosition && afterSepratorString.Length > 1)
                    {
                        e.Handled = true;
                    }

                }
            }
        }

        private void quickSettingsCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            Default -ZoomInOut, FadeIn Out, Rotate
            Zoom - RotateOut, Fade Out
            ZoomIn, RotateIn, Zoom Out
            RotateIn, RotateOut, Zoom 0ut
            */
            switch (quickSettingsCombobox.SelectedIndex)
            {
                case 0:
                    SetFadeIn(true, 2.0f);
                    SetFadeOut(true, 7.0f, 2.0f);
                    SetRotateIn(true, false, 720, 0.0f, 2.0f);
                    SetRotateOut(true, false, 720, 7.0f, 2.0f);
                    SetZoomIn(true, 135.0f, 25.0f, 0.0f);
                    SetZoomOut(true, 0.0f, 100.0f, 7.0f);
                    totalAnimationDurationTextbox.Text = "9";
                    break;

                case 1:
                    SetFadeIn(true, 0.1f);
                    SetFadeOut(true, 7.0f, 2.0f);
                    SetRotateIn(false, false, 720, 0.0f, 2.0f);
                    SetRotateOut(true, false, 720, 7.0f, 2.0f);
                    SetZoomIn(true, 135.0f, 50.0f, 0.0f);
                    SetZoomOut(true, 0.0f, 100.0f, 7.0f);
                    totalAnimationDurationTextbox.Text = "9";
                    break;

                case 2:
                    SetFadeIn(true, 0.1f);
                    SetFadeOut(true, 7.0f, 2.0f);
                    SetRotateIn(true, false, 720, 0.0f, 2.0f);
                    SetRotateOut(false, false, 720, 7.0f, 2.0f);
                    SetZoomIn(true, 135.0f, 50.0f, 0.0f);
                    SetZoomOut(true, 0.0f, 100.0f, 7.0f);
                    totalAnimationDurationTextbox.Text = "9";
                    break;

                case 3:
                    SetFadeIn(true, 0.1f);
                    SetFadeOut(true, 4.0f, 2.0f);
                    SetRotateIn(true, false, 720, 0.0f, 2.0f);
                    SetRotateOut(true, true, 720, 4.0f, 2.0f);
                    SetZoomIn(true, 135.0f, 100.0f, 0.0f);
                    SetZoomOut(true, 0.0f, 100.0f, 4.0f);
                    totalAnimationDurationTextbox.Text = "6";
                    break;

                default:
                    break;
            }
            if (quickSettingsCombobox.SelectedIndex == 0)
            {

            }
            
        }

        private void SetFadeIn(bool pUseFadeIn, float pFadeInDuration = 2.0f)
        {
            animFadeInCheckbox.Checked = pUseFadeIn;
            fadeInDurationTextbox.Text = pFadeInDuration.ToString();
        }

        private void SetFadeOut(bool pUseFadeOut, float pStartTime, float pDuration)
        {
            animFadeOutCheckbox.Checked = pUseFadeOut;
            fadeOutDurationTextbox.Text = pDuration.ToString();
            fadeOutTimeTextbox.Text = pStartTime.ToString();
        }

        private void SetRotateIn(bool pRoateIn, bool pCCW = false, int pRotateInDegrees = 720, float pRotInStart = 0.0f, float pRotInDur = 2.0f)
        {

            rotateInCheckbox.Checked = pRoateIn;
            rotateInClockwiseCheckbox.Checked = pCCW;
            rotateInDegreesTextbox.Text = pRotateInDegrees.ToString();
            rotateInStartTextbox.Text = pRotInStart.ToString();
            rotateInDurationTextbox.Text = pRotInDur.ToString();
        }

        private void SetRotateOut(bool pRotateOut, bool pCCW = false, int pRotOutDegrees = 720, float pRotOutStart = 6.0f, float pRotOutDur = 2.0f)
        {
            rotateOutCheckbox.Checked = pRotateOut;
            rotateOutClocwiseCheckbox.Checked = pCCW;
            rotateOutDegreesTextbox.Text = pRotOutDegrees.ToString();
            rotateOutStartTextbox.Text = pRotOutStart.ToString();
            rotateOutDurationTextbox.Text = pRotOutDur.ToString();
        }

        private void SetZoomIn(bool pZoomIn, float pZoomInSize = 135.0f, float pZoomInSpeed = 0.5f, float pZoomInStart = 2.0f)
        {
            zoomInCheckbox.Checked = pZoomIn;
            zoomInSizeTextbox.Text = pZoomInSize.ToString();
            zoomInSpeedTextbox.Text = pZoomInSpeed.ToString();
            zoomInStartTimeTextbox.Text = pZoomInStart.ToString();

        }

        private void SetZoomOut(bool pZoomOut, float pZoomOutSize = 0.0f, float pZoomOutSpeed = 0.2f, float pZoomOutStart = 2.0f)
        {
            zoomOutCheckbox.Checked = pZoomOut;
            zoomOutSizeTextbox.Text = pZoomOutSize.ToString();
            zoomOutSpeedTextbox.Text = pZoomOutSpeed.ToString();
            zoomOutStartTimeTextbox.Text = pZoomOutStart.ToString();
        }


        private bool alreadyExist(string _text, ref char KeyChar)
        {
            if (_text.IndexOf('.') > -1)
            {
                KeyChar = '.';
                return true;
            }
            if (_text.IndexOf(',') > -1)
            {
                KeyChar = ',';
                return true;
            }
            return false;
        }

        private void AnimationForm_Load(object sender, EventArgs e)
        {

        }
    }
}
