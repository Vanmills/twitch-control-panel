﻿namespace StreamOverlayControlPanel
{
    partial class StreamOverlayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StreamOverlayForm));
            this.turnOnOffTickerText = new System.Windows.Forms.Button();
            this.turnOnOffTickerButton = new System.Windows.Forms.Button();
            this.debugLogWindow = new System.Windows.Forms.TextBox();
            this.BgcolorButton = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.setTickerTextTextbox = new System.Windows.Forms.TextBox();
            this.setTickerTextButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.addImageButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tickerRepeatTimeSeconds = new System.Windows.Forms.TextBox();
            this.repeatTimeSecondsLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tickerTextSpeed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tickerBackgroundColorChange = new System.Windows.Forms.Button();
            this.tickerLabelPanel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.backgroundLabel = new System.Windows.Forms.Label();
            this.debugTextbox = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unityControlsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expandButton = new System.Windows.Forms.Button();
            this.presetList = new System.Windows.Forms.ComboBox();
            this.presetLabel = new System.Windows.Forms.Label();
            this.addPresetButton = new System.Windows.Forms.Button();
            this.removePresetButton = new System.Windows.Forms.Button();
            this.presetColorDialog = new System.Windows.Forms.ColorDialog();
            this.presetTickerBackgroundColorDialog = new System.Windows.Forms.ColorDialog();
            this.presetTickerTextColorDialog = new System.Windows.Forms.ColorDialog();
            this.trayPanel = new System.Windows.Forms.Panel();
            this.addTrayGraphicsButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.trayTextColorDialog = new System.Windows.Forms.ColorDialog();
            this.colorTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.addAnimatedImageButton = new System.Windows.Forms.Button();
            this.animatedImagesCombobox = new System.Windows.Forms.ComboBox();
            this.animatedImagesLabel = new System.Windows.Forms.Label();
            this.playAnimationButton = new System.Windows.Forms.Button();
            this.versionLabel = new System.Windows.Forms.Label();
            this.usePresetButton = new System.Windows.Forms.Button();
            this.saveAnimationListButton = new System.Windows.Forms.Button();
            this.removeAnimationButton = new System.Windows.Forms.Button();
            this.savePresetButton = new System.Windows.Forms.Button();
            this.editPresetButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // turnOnOffTickerText
            // 
            this.turnOnOffTickerText.Location = new System.Drawing.Point(190, 136);
            this.turnOnOffTickerText.Name = "turnOnOffTickerText";
            this.turnOnOffTickerText.Size = new System.Drawing.Size(136, 20);
            this.turnOnOffTickerText.TabIndex = 7;
            this.turnOnOffTickerText.Text = "Ticker Text On/Off";
            this.turnOnOffTickerText.UseVisualStyleBackColor = true;
            this.turnOnOffTickerText.Click += new System.EventHandler(this.actorButton_Click);
            // 
            // turnOnOffTickerButton
            // 
            this.turnOnOffTickerButton.Location = new System.Drawing.Point(6, 136);
            this.turnOnOffTickerButton.Name = "turnOnOffTickerButton";
            this.turnOnOffTickerButton.Size = new System.Drawing.Size(131, 20);
            this.turnOnOffTickerButton.TabIndex = 6;
            this.turnOnOffTickerButton.Text = "Ticker bar On/Off";
            this.turnOnOffTickerButton.UseVisualStyleBackColor = true;
            this.turnOnOffTickerButton.Click += new System.EventHandler(this.actorButton2_Click);
            // 
            // debugLogWindow
            // 
            this.debugLogWindow.AcceptsReturn = true;
            this.debugLogWindow.Location = new System.Drawing.Point(367, 497);
            this.debugLogWindow.Multiline = true;
            this.debugLogWindow.Name = "debugLogWindow";
            this.debugLogWindow.Size = new System.Drawing.Size(272, 38);
            this.debugLogWindow.TabIndex = 20;
            this.debugLogWindow.TabStop = false;
            // 
            // BgcolorButton
            // 
            this.BgcolorButton.Location = new System.Drawing.Point(190, 24);
            this.BgcolorButton.Name = "BgcolorButton";
            this.BgcolorButton.Size = new System.Drawing.Size(130, 28);
            this.BgcolorButton.TabIndex = 10;
            this.BgcolorButton.Text = "Set Background color";
            this.BgcolorButton.UseVisualStyleBackColor = true;
            this.BgcolorButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(190, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 20);
            this.button2.TabIndex = 5;
            this.button2.Text = "Ticker Text Color";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // setTickerTextTextbox
            // 
            this.setTickerTextTextbox.Location = new System.Drawing.Point(10, 69);
            this.setTickerTextTextbox.Name = "setTickerTextTextbox";
            this.setTickerTextTextbox.Size = new System.Drawing.Size(236, 20);
            this.setTickerTextTextbox.TabIndex = 2;
            // 
            // setTickerTextButton
            // 
            this.setTickerTextButton.Location = new System.Drawing.Point(252, 25);
            this.setTickerTextButton.Name = "setTickerTextButton";
            this.setTickerTextButton.Size = new System.Drawing.Size(74, 64);
            this.setTickerTextButton.TabIndex = 3;
            this.setTickerTextButton.Text = "Set TickerText";
            this.setTickerTextButton.UseVisualStyleBackColor = true;
            this.setTickerTextButton.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(495, 481);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Debug box. Will be removed";
            // 
            // addImageButton
            // 
            this.addImageButton.Location = new System.Drawing.Point(6, 26);
            this.addImageButton.Name = "addImageButton";
            this.addImageButton.Size = new System.Drawing.Size(130, 26);
            this.addImageButton.TabIndex = 9;
            this.addImageButton.Text = "Add image";
            this.addImageButton.UseVisualStyleBackColor = true;
            this.addImageButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tickerRepeatTimeSeconds);
            this.panel1.Controls.Add(this.repeatTimeSecondsLabel);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tickerTextSpeed);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tickerBackgroundColorChange);
            this.panel1.Controls.Add(this.turnOnOffTickerText);
            this.panel1.Controls.Add(this.setTickerTextButton);
            this.panel1.Controls.Add(this.turnOnOffTickerButton);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.setTickerTextTextbox);
            this.panel1.Location = new System.Drawing.Point(1, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(338, 172);
            this.panel1.TabIndex = 0;
            // 
            // tickerRepeatTimeSeconds
            // 
            this.tickerRepeatTimeSeconds.Location = new System.Drawing.Point(131, 22);
            this.tickerRepeatTimeSeconds.Name = "tickerRepeatTimeSeconds";
            this.tickerRepeatTimeSeconds.Size = new System.Drawing.Size(44, 20);
            this.tickerRepeatTimeSeconds.TabIndex = 1;
            this.tickerRepeatTimeSeconds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // repeatTimeSecondsLabel
            // 
            this.repeatTimeSecondsLabel.AutoSize = true;
            this.repeatTimeSecondsLabel.Location = new System.Drawing.Point(128, 6);
            this.repeatTimeSecondsLabel.Name = "repeatTimeSecondsLabel";
            this.repeatTimeSecondsLabel.Size = new System.Drawing.Size(64, 13);
            this.repeatTimeSecondsLabel.TabIndex = 12;
            this.repeatTimeSecondsLabel.Text = "Repeat time";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Ticker Text Speed";
            // 
            // tickerTextSpeed
            // 
            this.tickerTextSpeed.Location = new System.Drawing.Point(10, 22);
            this.tickerTextSpeed.Name = "tickerTextSpeed";
            this.tickerTextSpeed.Size = new System.Drawing.Size(41, 20);
            this.tickerTextSpeed.TabIndex = 0;
            this.tickerTextSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Ticker Text:";
            // 
            // tickerBackgroundColorChange
            // 
            this.tickerBackgroundColorChange.Location = new System.Drawing.Point(6, 107);
            this.tickerBackgroundColorChange.Name = "tickerBackgroundColorChange";
            this.tickerBackgroundColorChange.Size = new System.Drawing.Size(132, 20);
            this.tickerBackgroundColorChange.TabIndex = 4;
            this.tickerBackgroundColorChange.Text = "Ticker Background color";
            this.tickerBackgroundColorChange.UseVisualStyleBackColor = true;
            this.tickerBackgroundColorChange.Click += new System.EventHandler(this.tickerBackgroundColorChange_Click);
            // 
            // tickerLabelPanel
            // 
            this.tickerLabelPanel.AutoSize = true;
            this.tickerLabelPanel.Location = new System.Drawing.Point(3, 31);
            this.tickerLabelPanel.Name = "tickerLabelPanel";
            this.tickerLabelPanel.Size = new System.Drawing.Size(78, 13);
            this.tickerLabelPanel.TabIndex = 7;
            this.tickerLabelPanel.Text = "Ticker Settings";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.backgroundLabel);
            this.panel2.Controls.Add(this.addImageButton);
            this.panel2.Controls.Add(this.BgcolorButton);
            this.panel2.Location = new System.Drawing.Point(1, 231);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(338, 63);
            this.panel2.TabIndex = 8;
            // 
            // backgroundLabel
            // 
            this.backgroundLabel.AutoSize = true;
            this.backgroundLabel.Location = new System.Drawing.Point(3, 0);
            this.backgroundLabel.Name = "backgroundLabel";
            this.backgroundLabel.Size = new System.Drawing.Size(157, 13);
            this.backgroundLabel.TabIndex = 9;
            this.backgroundLabel.Text = "Background and image controls";
            // 
            // debugTextbox
            // 
            this.debugTextbox.AutoSize = true;
            this.debugTextbox.Location = new System.Drawing.Point(367, 481);
            this.debugTextbox.Name = "debugTextbox";
            this.debugTextbox.Size = new System.Drawing.Size(84, 13);
            this.debugTextbox.TabIndex = 14;
            this.debugTextbox.Text = "Debug Text Box";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(644, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.unityControlsToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem1.Text = "?";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // unityControlsToolStripMenuItem
            // 
            this.unityControlsToolStripMenuItem.Name = "unityControlsToolStripMenuItem";
            this.unityControlsToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.unityControlsToolStripMenuItem.Text = "Unity Controls";
            this.unityControlsToolStripMenuItem.Click += new System.EventHandler(this.unityControlsToolStripMenuItem_Click);
            // 
            // expandButton
            // 
            this.expandButton.Location = new System.Drawing.Point(324, 551);
            this.expandButton.Name = "expandButton";
            this.expandButton.Size = new System.Drawing.Size(15, 29);
            this.expandButton.TabIndex = 19;
            this.expandButton.Text = "<";
            this.expandButton.UseVisualStyleBackColor = true;
            this.expandButton.Click += new System.EventHandler(this.expandButton_Click);
            // 
            // presetList
            // 
            this.presetList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.presetList.FormattingEnabled = true;
            this.presetList.Location = new System.Drawing.Point(369, 58);
            this.presetList.Name = "presetList";
            this.presetList.Size = new System.Drawing.Size(180, 21);
            this.presetList.TabIndex = 17;
            // 
            // presetLabel
            // 
            this.presetLabel.AutoSize = true;
            this.presetLabel.Location = new System.Drawing.Point(369, 42);
            this.presetLabel.Name = "presetLabel";
            this.presetLabel.Size = new System.Drawing.Size(42, 13);
            this.presetLabel.TabIndex = 20;
            this.presetLabel.Text = "Presets";
            this.presetLabel.Visible = false;
            // 
            // addPresetButton
            // 
            this.addPresetButton.Location = new System.Drawing.Point(369, 94);
            this.addPresetButton.Name = "addPresetButton";
            this.addPresetButton.Size = new System.Drawing.Size(95, 23);
            this.addPresetButton.TabIndex = 16;
            this.addPresetButton.Text = "Add Preset";
            this.addPresetButton.UseVisualStyleBackColor = true;
            this.addPresetButton.Click += new System.EventHandler(this.addPresetButton_Click);
            // 
            // removePresetButton
            // 
            this.removePresetButton.Location = new System.Drawing.Point(537, 129);
            this.removePresetButton.Name = "removePresetButton";
            this.removePresetButton.Size = new System.Drawing.Size(95, 23);
            this.removePresetButton.TabIndex = 22;
            this.removePresetButton.TabStop = false;
            this.removePresetButton.Text = "Remove Preset";
            this.removePresetButton.UseVisualStyleBackColor = true;
            // 
            // trayPanel
            // 
            this.trayPanel.AutoScroll = true;
            this.trayPanel.AutoScrollMargin = new System.Drawing.Size(10, 0);
            this.trayPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trayPanel.Location = new System.Drawing.Point(1, 414);
            this.trayPanel.Name = "trayPanel";
            this.trayPanel.Size = new System.Drawing.Size(338, 121);
            this.trayPanel.TabIndex = 34;
            // 
            // addTrayGraphicsButton
            // 
            this.addTrayGraphicsButton.Location = new System.Drawing.Point(6, 540);
            this.addTrayGraphicsButton.Name = "addTrayGraphicsButton";
            this.addTrayGraphicsButton.Size = new System.Drawing.Size(114, 43);
            this.addTrayGraphicsButton.TabIndex = 15;
            this.addTrayGraphicsButton.Text = "Add Tray Graphics";
            this.addTrayGraphicsButton.UseVisualStyleBackColor = true;
            this.addTrayGraphicsButton.Click += new System.EventHandler(this.addTrayGraphicsButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 398);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Tray Settings Area";
            // 
            // colorTooltip
            // 
            this.colorTooltip.ToolTipTitle = "Tray Information";
            // 
            // addAnimatedImageButton
            // 
            this.addAnimatedImageButton.Location = new System.Drawing.Point(12, 360);
            this.addAnimatedImageButton.Name = "addAnimatedImageButton";
            this.addAnimatedImageButton.Size = new System.Drawing.Size(97, 23);
            this.addAnimatedImageButton.TabIndex = 11;
            this.addAnimatedImageButton.Text = "Add Animation";
            this.addAnimatedImageButton.UseVisualStyleBackColor = true;
            this.addAnimatedImageButton.Click += new System.EventHandler(this.addAnimatedImageButton_Click);
            // 
            // animatedImagesCombobox
            // 
            this.animatedImagesCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.animatedImagesCombobox.FormattingEnabled = true;
            this.animatedImagesCombobox.Location = new System.Drawing.Point(12, 326);
            this.animatedImagesCombobox.Name = "animatedImagesCombobox";
            this.animatedImagesCombobox.Size = new System.Drawing.Size(198, 21);
            this.animatedImagesCombobox.TabIndex = 13;
            // 
            // animatedImagesLabel
            // 
            this.animatedImagesLabel.AutoSize = true;
            this.animatedImagesLabel.Location = new System.Drawing.Point(13, 310);
            this.animatedImagesLabel.Name = "animatedImagesLabel";
            this.animatedImagesLabel.Size = new System.Drawing.Size(91, 13);
            this.animatedImagesLabel.TabIndex = 40;
            this.animatedImagesLabel.Text = "Animated Images:";
            // 
            // playAnimationButton
            // 
            this.playAnimationButton.Location = new System.Drawing.Point(228, 324);
            this.playAnimationButton.Name = "playAnimationButton";
            this.playAnimationButton.Size = new System.Drawing.Size(100, 23);
            this.playAnimationButton.TabIndex = 14;
            this.playAnimationButton.Text = "Play Selected Animation";
            this.playAnimationButton.UseVisualStyleBackColor = true;
            this.playAnimationButton.Click += new System.EventHandler(this.playAnimationButton_Click);
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Location = new System.Drawing.Point(558, 570);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(72, 13);
            this.versionLabel.TabIndex = 10;
            this.versionLabel.Text = "Version 0.48a";
            // 
            // usePresetButton
            // 
            this.usePresetButton.Location = new System.Drawing.Point(558, 57);
            this.usePresetButton.Name = "usePresetButton";
            this.usePresetButton.Size = new System.Drawing.Size(74, 23);
            this.usePresetButton.TabIndex = 18;
            this.usePresetButton.Text = "Use Preset";
            this.usePresetButton.UseVisualStyleBackColor = true;
            this.usePresetButton.Click += new System.EventHandler(this.usePresetButton_Click);
            // 
            // saveAnimationListButton
            // 
            this.saveAnimationListButton.Location = new System.Drawing.Point(228, 360);
            this.saveAnimationListButton.Name = "saveAnimationListButton";
            this.saveAnimationListButton.Size = new System.Drawing.Size(100, 23);
            this.saveAnimationListButton.TabIndex = 12;
            this.saveAnimationListButton.Text = "Save Animations";
            this.saveAnimationListButton.UseVisualStyleBackColor = true;
            this.saveAnimationListButton.Click += new System.EventHandler(this.saveAnimationListButton_Click);
            // 
            // removeAnimationButton
            // 
            this.removeAnimationButton.Location = new System.Drawing.Point(115, 360);
            this.removeAnimationButton.Name = "removeAnimationButton";
            this.removeAnimationButton.Size = new System.Drawing.Size(107, 23);
            this.removeAnimationButton.TabIndex = 45;
            this.removeAnimationButton.TabStop = false;
            this.removeAnimationButton.Text = "Remove Animation";
            this.removeAnimationButton.UseVisualStyleBackColor = true;
            this.removeAnimationButton.Click += new System.EventHandler(this.removeAnimationButton_Click);
            // 
            // savePresetButton
            // 
            this.savePresetButton.Location = new System.Drawing.Point(369, 129);
            this.savePresetButton.Name = "savePresetButton";
            this.savePresetButton.Size = new System.Drawing.Size(95, 23);
            this.savePresetButton.TabIndex = 19;
            this.savePresetButton.Text = "Save Presets";
            this.savePresetButton.UseVisualStyleBackColor = true;
            this.savePresetButton.Click += new System.EventHandler(this.savePresetButton_Click);
            // 
            // editPresetButton
            // 
            this.editPresetButton.Location = new System.Drawing.Point(537, 94);
            this.editPresetButton.Name = "editPresetButton";
            this.editPresetButton.Size = new System.Drawing.Size(95, 23);
            this.editPresetButton.TabIndex = 47;
            this.editPresetButton.Text = "Edit Preset";
            this.editPresetButton.UseVisualStyleBackColor = true;
            this.editPresetButton.Click += new System.EventHandler(this.editPresetButton_Click);
            // 
            // StreamOverlayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 592);
            this.Controls.Add(this.editPresetButton);
            this.Controls.Add(this.savePresetButton);
            this.Controls.Add(this.removeAnimationButton);
            this.Controls.Add(this.saveAnimationListButton);
            this.Controls.Add(this.usePresetButton);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.playAnimationButton);
            this.Controls.Add(this.animatedImagesLabel);
            this.Controls.Add(this.animatedImagesCombobox);
            this.Controls.Add(this.addAnimatedImageButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addTrayGraphicsButton);
            this.Controls.Add(this.trayPanel);
            this.Controls.Add(this.removePresetButton);
            this.Controls.Add(this.addPresetButton);
            this.Controls.Add(this.presetLabel);
            this.Controls.Add(this.presetList);
            this.Controls.Add(this.expandButton);
            this.Controls.Add(this.debugTextbox);
            this.Controls.Add(this.tickerLabelPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.debugLogWindow);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(660, 631);
            this.MinimumSize = new System.Drawing.Size(360, 631);
            this.Name = "StreamOverlayForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Stream Overlay Controlpanel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button turnOnOffTickerText;
        private System.Windows.Forms.Button turnOnOffTickerButton;
        private System.Windows.Forms.TextBox debugLogWindow;
        private System.Windows.Forms.Button BgcolorButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox setTickerTextTextbox;
        private System.Windows.Forms.Button setTickerTextButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addImageButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label tickerLabelPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label backgroundLabel;
        private System.Windows.Forms.Button tickerBackgroundColorChange;
        private System.Windows.Forms.Label debugTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button expandButton;
        private System.Windows.Forms.ComboBox presetList;
        private System.Windows.Forms.Label presetLabel;
        private System.Windows.Forms.Button addPresetButton;
        private System.Windows.Forms.Button removePresetButton;
        private System.Windows.Forms.ColorDialog presetColorDialog;
        private System.Windows.Forms.ColorDialog presetTickerBackgroundColorDialog;
        private System.Windows.Forms.ColorDialog presetTickerTextColorDialog;
        private System.Windows.Forms.ToolStripMenuItem unityControlsToolStripMenuItem;
        private System.Windows.Forms.Panel trayPanel;
        private System.Windows.Forms.Button addTrayGraphicsButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColorDialog trayTextColorDialog;
        private System.Windows.Forms.ToolTip colorTooltip;
        private System.Windows.Forms.Button addAnimatedImageButton;
        private System.Windows.Forms.ComboBox animatedImagesCombobox;
        private System.Windows.Forms.Label animatedImagesLabel;
        private System.Windows.Forms.Button playAnimationButton;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Button usePresetButton;
        private System.Windows.Forms.Button saveAnimationListButton;
        private System.Windows.Forms.Button removeAnimationButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tickerTextSpeed;
        private System.Windows.Forms.TextBox tickerRepeatTimeSeconds;
        private System.Windows.Forms.Label repeatTimeSecondsLabel;
        private System.Windows.Forms.Button savePresetButton;
        private System.Windows.Forms.Button editPresetButton;
    }
}

