﻿using System.Windows.Forms;
using StreamOverlayControlPanel;

// Should rename this to TrayButton

public class IndexedButton : System.Windows.Forms.Button
{
    public int ButtonID { get; set; }
    //public Tray TrayInformation;

    public IndexedButton() : base()
    {
        ButtonID = -1;
    }
}