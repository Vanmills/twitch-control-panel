﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StreamOverlayControlPanel
{
    public partial class PresetForm : Form
    {
        Preset preset;
        private int trayID = 0;

        private PresetForm()
        {
            InitializeComponent();
        }

        public PresetForm(Preset pPreset, bool loadForm = false)
        {
            InitializeComponent();
            preset = pPreset;
            tickerSpeedTextbox.Text = preset.tickerSpeed.ToString();
            tickerRepeatTimeTextbox.Text = preset.tickerRepeatTime.ToString();
            if (loadForm)
            {
                addPresetButton.Text = "Change Preset";
                LoadFormData();
            }
        }

        private void LoadFormData()
        {
            presetSetNameTextBox.Text = preset.presetName;
            presetSelectedBackgroundColorPicturebox.BackColor = preset.backgroundColor.GetWindowsColor();
            presetTickerBackgroundColorPicturebox.BackColor = preset.backgroundColor.GetWindowsColor();
            presetTickerTextPicturebox.BackColor = preset.tickerTextColor.GetWindowsColor();
            tickerTextTextbox.Text = preset.tickerText;
            tickerSpeedTextbox.Text = preset.tickerSpeed.ToString();
            tickerRepeatTimeTextbox.Text = preset.tickerRepeatTime.ToString();
            for (int i = 0; i < preset.gameImages.Count; i++)
            {
                imageListCombobox.Items.Add(Path.GetFileNameWithoutExtension(preset.gameImages[i]));
            }
            for (int i = 0; i < preset.listOfTrays.Count; i++)
            {
                traysListCombobox.Items.Add(Path.GetFileNameWithoutExtension(preset.listOfTrays[i].trayFilename));
            }
        }

        private void addPresetButton_Click(object sender, EventArgs e)
        {
            // form validation should go here first. 

            //preset.backgroundColor = presetSelectedBackgroundColorPicturebox.BackColor;
            //preset.tickerBackgroundColor = presetTickerBackgroundColorPicturebox.BackColor;
            //preset.tickerTextColor = presetTickerTextPicturebox.BackColor;
            preset.presetName = presetSetNameTextBox.Text;
            preset.tickerText = tickerTextTextbox.Text;
            if (!string.IsNullOrEmpty(tickerSpeedTextbox.Text))
            {
                preset.tickerSpeed = float.Parse(tickerSpeedTextbox.Text);
            }
            else
            {
                preset.tickerSpeed = 20.0f;
            }
            if (!string.IsNullOrEmpty(tickerRepeatTimeTextbox.Text))
            {
                preset.tickerRepeatTime = int.Parse(tickerRepeatTimeTextbox.Text);
            }
            else
            {
                preset.tickerRepeatTime = 35.0f;
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void addTraysButton_Click(object sender, EventArgs e)
        {
            Tray tray = new Tray();
            tray.trayID = trayID;
            trayID++;
            AddTrayForm trayForm = new AddTrayForm(tray);
            DialogResult result = trayForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                preset.listOfTrays.Add(tray);
                traysListCombobox.Items.Add(Path.GetFileNameWithoutExtension(tray.trayFilename));
                traysListCombobox.SelectedItem = Path.GetFileNameWithoutExtension(tray.trayFilename);

            }
        }

        private void addImagesButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Graphics (*.png; *.jpg) | *.jpg;*.png";
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                preset.gameImages.Add(fileDialog.FileName);
                imageListCombobox.Items.Add(Path.GetFileNameWithoutExtension(fileDialog.FileName));
                imageListCombobox.SelectedItem = Path.GetFileNameWithoutExtension(fileDialog.FileName);
            }
        }

        private void presetBackgroundColorButton_Click(object sender, EventArgs e)
        {
            DialogResult result = backgroundColorPicker.ShowDialog();
            if (result == DialogResult.OK)
            {
                presetSelectedBackgroundColorPicturebox.BackColor = backgroundColorPicker.Color;
                preset.backgroundColor = UserColor.SetColor(backgroundColorPicker.Color);
            }
        }

        private void presetTickerBackground_Click(object sender, EventArgs e)
        {
            DialogResult result = tickerBackgroundColorPicker.ShowDialog();
            if (result == DialogResult.OK)
            {
                preset.tickerBackgroundColor = UserColor.SetColor(tickerBackgroundColorPicker.Color);
                presetTickerBackgroundColorPicturebox.BackColor = tickerBackgroundColorPicker.Color;
            }
        }

        private void presetTickerTextColorButton_Click(object sender, EventArgs e)
        {
            DialogResult result = tickerTextColorPicker.ShowDialog();
            if (result == DialogResult.OK)
            {
                preset.tickerTextColor = UserColor.SetColor(tickerTextColorPicker.Color);
                presetTickerTextPicturebox.BackColor = tickerTextColorPicker.Color;
            }
        }

        private void tickerBGOnOff_CheckedChanged(object sender, EventArgs e)
        {
            presetTickerBackground.Visible = tickerBGOnOff.Checked;
            presetTickerBackgroundColorPicturebox.Visible = tickerBGOnOff.Checked;
        }

        private void tickerTextOnOff_CheckedChanged(object sender, EventArgs e)
        {
            presetTickerTextPicturebox.Visible = tickerTextOnOff.Checked;
            presetTickerTextColorButton.Visible = tickerTextOnOff.Checked;
            tickerTextTextbox.Visible = tickerTextOnOff.Checked;
            presetTickerTextLabel.Visible = tickerTextOnOff.Checked;
        }

        private void validateData(object sender, KeyPressEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            //fileNameLabel.Text = textBox.Name;

            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar.Equals(',') && e.KeyChar.Equals('.');
            if (!char.IsControl(e.KeyChar) &&
                !char.IsDigit(e.KeyChar) &&
                e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            //check if '.' , ',' pressed
            char sepratorChar = ',';
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
            if (e.KeyChar == ',')
            {
                // check if it's in the beginning of text not accept
                if (textBox.Text.Length == 0) e.Handled = true;
                // check if it's in the beginning of text not accept
                if (textBox.SelectionStart == 0) e.Handled = true;
                // check if there is already exist a '.' , ','
                if (alreadyExist(textBox.Text, ref sepratorChar)) e.Handled = true;
                //check if '.' or ',' is in middle of a number and after it is not a number greater than 99
                if (textBox.SelectionStart != textBox.Text.Length && e.Handled == false)
                {
                    // '.' or ',' is in the middle
                    string AfterDotString = textBox.Text.Substring(textBox.SelectionStart);

                    if (AfterDotString.Length > 2)
                    {
                        e.Handled = true;
                    }
                }
            }
            //check if a number pressed

            if (Char.IsDigit(e.KeyChar))
            {
                //check if a coma or dot exist
                if (alreadyExist(textBox.Text, ref sepratorChar))
                {
                    int sepratorPosition = textBox.Text.IndexOf(sepratorChar);
                    string afterSepratorString = textBox.Text.Substring(sepratorPosition + 1);
                    if (textBox.SelectionStart > sepratorPosition && afterSepratorString.Length > 1)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private bool alreadyExist(string _text, ref char KeyChar)
        {
            if (_text.IndexOf('.') > -1)
            {
                KeyChar = '.';
                return true;
            }
            if (_text.IndexOf(',') > -1)
            {
                KeyChar = ',';
                return true;
            }
            return false;
        }

        private void removeImageFromList_Click(object sender, EventArgs e)
        {
            if (imageListCombobox.SelectedIndex != -1)
            {
                int currentSelection = imageListCombobox.SelectedIndex;
                preset.gameImages.Remove(preset.gameImages[traysListCombobox.SelectedIndex]);
                imageListCombobox.Items.Remove(imageListCombobox.SelectedItem);
                imageListCombobox.SelectedIndex = currentSelection - 1;
                imageListCombobox.Refresh();

            }
        }

        private void removeSelectedTrayButton_Click(object sender, EventArgs e)
        {
            if (traysListCombobox.SelectedIndex != -1)
            {
                int currentSelection = traysListCombobox.SelectedIndex;
                preset.listOfTrays.Remove(preset.listOfTrays[traysListCombobox.SelectedIndex]);
                traysListCombobox.Items.Remove(traysListCombobox.SelectedItem);
                traysListCombobox.SelectedIndex = currentSelection -1;
                traysListCombobox.Refresh();

            }
        }

        private void PresetForm_Load(object sender, EventArgs e)
        {

        }

        private void PresetForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
