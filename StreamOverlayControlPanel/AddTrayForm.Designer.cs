﻿namespace StreamOverlayControlPanel
{
    partial class AddTrayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trayAddGraphics = new System.Windows.Forms.Button();
            this.trayTextLabel = new System.Windows.Forms.Label();
            this.trayTextTextbox = new System.Windows.Forms.TextBox();
            this.trayFadeInSpeedLabel = new System.Windows.Forms.Label();
            this.trayFadeInTextbox = new System.Windows.Forms.TextBox();
            this.trayTextColorLabel = new System.Windows.Forms.Label();
            this.textColorDialog = new System.Windows.Forms.ColorDialog();
            this.traySelectedColorPictureBox = new System.Windows.Forms.PictureBox();
            this.trayFadeOutTextbox = new System.Windows.Forms.TextBox();
            this.trayFadeOutSpeedLabel = new System.Windows.Forms.Label();
            this.trayAnimationDirectionCombobox = new System.Windows.Forms.ComboBox();
            this.trayAnimDirectionLabel = new System.Windows.Forms.Label();
            this.traySelectColorButton = new System.Windows.Forms.Button();
            this.trayUseCountdownLabel = new System.Windows.Forms.Label();
            this.trayUseCountdownCheckbox = new System.Windows.Forms.CheckBox();
            this.trayMinutesComboBox = new System.Windows.Forms.ComboBox();
            this.traySelectedColorLabel = new System.Windows.Forms.Label();
            this.trayUseTimerCheckbox = new System.Windows.Forms.CheckBox();
            this.trayUseTimerLabel = new System.Windows.Forms.Label();
            this.addTrayTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.trayAutoAnimateLabel = new System.Windows.Forms.Label();
            this.trayAutoAnimateCheckbox = new System.Windows.Forms.CheckBox();
            this.trayIntervalLabel = new System.Windows.Forms.Label();
            this.trayAutoAnimateIntervalFromTextbox = new System.Windows.Forms.TextBox();
            this.trayAutoAnimateToLabel = new System.Windows.Forms.Label();
            this.trayAutoAnimateIntervalToTextbox = new System.Windows.Forms.TextBox();
            this.trayCountdownMinutesLabel = new System.Windows.Forms.Label();
            this.traySaveAndCloseButton = new System.Windows.Forms.Button();
            this.trayGraphicsDialogBox = new System.Windows.Forms.OpenFileDialog();
            this.trayCloseWithoutSaveButton = new System.Windows.Forms.Button();
            this.trayFilenameLabel = new System.Windows.Forms.Label();
            this.trayFileNameLabelText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.trayRandomIntervalInfoLabel = new System.Windows.Forms.Label();
            this.trayAutoAnimateFadeOutToTextbox = new System.Windows.Forms.TextBox();
            this.trayFadeoutToLabel = new System.Windows.Forms.Label();
            this.trayAutoAnimateFadeOutFromTextbox = new System.Windows.Forms.TextBox();
            this.trayIntervalFadeOutLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.traySelectedColorPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // trayAddGraphics
            // 
            this.trayAddGraphics.Location = new System.Drawing.Point(14, 23);
            this.trayAddGraphics.Name = "trayAddGraphics";
            this.trayAddGraphics.Size = new System.Drawing.Size(128, 23);
            this.trayAddGraphics.TabIndex = 0;
            this.trayAddGraphics.Text = "Select Tray Graphics";
            this.trayAddGraphics.UseVisualStyleBackColor = true;
            this.trayAddGraphics.Click += new System.EventHandler(this.trayAddGraphics_Click);
            // 
            // trayTextLabel
            // 
            this.trayTextLabel.AutoSize = true;
            this.trayTextLabel.Location = new System.Drawing.Point(12, 64);
            this.trayTextLabel.Name = "trayTextLabel";
            this.trayTextLabel.Size = new System.Drawing.Size(28, 13);
            this.trayTextLabel.TabIndex = 1;
            this.trayTextLabel.Text = "Text";
            // 
            // trayTextTextbox
            // 
            this.trayTextTextbox.Location = new System.Drawing.Point(93, 61);
            this.trayTextTextbox.Multiline = true;
            this.trayTextTextbox.Name = "trayTextTextbox";
            this.trayTextTextbox.Size = new System.Drawing.Size(235, 85);
            this.trayTextTextbox.TabIndex = 2;
            // 
            // trayFadeInSpeedLabel
            // 
            this.trayFadeInSpeedLabel.AutoSize = true;
            this.trayFadeInSpeedLabel.Location = new System.Drawing.Point(11, 204);
            this.trayFadeInSpeedLabel.Name = "trayFadeInSpeedLabel";
            this.trayFadeInSpeedLabel.Size = new System.Drawing.Size(80, 13);
            this.trayFadeInSpeedLabel.TabIndex = 3;
            this.trayFadeInSpeedLabel.Text = "Fade In Speed:";
            // 
            // trayFadeInTextbox
            // 
            this.trayFadeInTextbox.Location = new System.Drawing.Point(93, 200);
            this.trayFadeInTextbox.Name = "trayFadeInTextbox";
            this.trayFadeInTextbox.Size = new System.Drawing.Size(45, 20);
            this.trayFadeInTextbox.TabIndex = 4;
            this.trayFadeInTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // trayTextColorLabel
            // 
            this.trayTextColorLabel.AutoSize = true;
            this.trayTextColorLabel.Location = new System.Drawing.Point(12, 178);
            this.trayTextColorLabel.Name = "trayTextColorLabel";
            this.trayTextColorLabel.Size = new System.Drawing.Size(55, 13);
            this.trayTextColorLabel.TabIndex = 5;
            this.trayTextColorLabel.Text = "Text Color";
            // 
            // traySelectedColorPictureBox
            // 
            this.traySelectedColorPictureBox.Location = new System.Drawing.Point(263, 173);
            this.traySelectedColorPictureBox.Name = "traySelectedColorPictureBox";
            this.traySelectedColorPictureBox.Size = new System.Drawing.Size(65, 20);
            this.traySelectedColorPictureBox.TabIndex = 6;
            this.traySelectedColorPictureBox.TabStop = false;
            // 
            // trayFadeOutTextbox
            // 
            this.trayFadeOutTextbox.Location = new System.Drawing.Point(268, 201);
            this.trayFadeOutTextbox.Name = "trayFadeOutTextbox";
            this.trayFadeOutTextbox.Size = new System.Drawing.Size(60, 20);
            this.trayFadeOutTextbox.TabIndex = 8;
            this.trayFadeOutTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // trayFadeOutSpeedLabel
            // 
            this.trayFadeOutSpeedLabel.AutoSize = true;
            this.trayFadeOutSpeedLabel.Location = new System.Drawing.Point(177, 204);
            this.trayFadeOutSpeedLabel.Name = "trayFadeOutSpeedLabel";
            this.trayFadeOutSpeedLabel.Size = new System.Drawing.Size(85, 13);
            this.trayFadeOutSpeedLabel.TabIndex = 7;
            this.trayFadeOutSpeedLabel.Text = "Fade Out Speed";
            // 
            // trayAnimationDirectionCombobox
            // 
            this.trayAnimationDirectionCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.trayAnimationDirectionCombobox.FormattingEnabled = true;
            this.trayAnimationDirectionCombobox.Items.AddRange(new object[] {
            "Up",
            "Down",
            "Left",
            "Right"});
            this.trayAnimationDirectionCombobox.Location = new System.Drawing.Point(192, 227);
            this.trayAnimationDirectionCombobox.Name = "trayAnimationDirectionCombobox";
            this.trayAnimationDirectionCombobox.Size = new System.Drawing.Size(136, 21);
            this.trayAnimationDirectionCombobox.TabIndex = 9;
            // 
            // trayAnimDirectionLabel
            // 
            this.trayAnimDirectionLabel.AutoSize = true;
            this.trayAnimDirectionLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.trayAnimDirectionLabel.Location = new System.Drawing.Point(11, 230);
            this.trayAnimDirectionLabel.Name = "trayAnimDirectionLabel";
            this.trayAnimDirectionLabel.Size = new System.Drawing.Size(96, 13);
            this.trayAnimDirectionLabel.TabIndex = 10;
            this.trayAnimDirectionLabel.Text = "Animation direction";
            // 
            // traySelectColorButton
            // 
            this.traySelectColorButton.Location = new System.Drawing.Point(93, 171);
            this.traySelectColorButton.Name = "traySelectColorButton";
            this.traySelectColorButton.Size = new System.Drawing.Size(73, 23);
            this.traySelectColorButton.TabIndex = 11;
            this.traySelectColorButton.Text = "Select Color";
            this.traySelectColorButton.UseVisualStyleBackColor = true;
            this.traySelectColorButton.Click += new System.EventHandler(this.traySelectColorButton_Click);
            // 
            // trayUseCountdownLabel
            // 
            this.trayUseCountdownLabel.AutoSize = true;
            this.trayUseCountdownLabel.Location = new System.Drawing.Point(12, 259);
            this.trayUseCountdownLabel.Name = "trayUseCountdownLabel";
            this.trayUseCountdownLabel.Size = new System.Drawing.Size(86, 13);
            this.trayUseCountdownLabel.TabIndex = 12;
            this.trayUseCountdownLabel.Text = "Use Countdown:";
            // 
            // trayUseCountdownCheckbox
            // 
            this.trayUseCountdownCheckbox.AutoSize = true;
            this.trayUseCountdownCheckbox.Location = new System.Drawing.Point(115, 258);
            this.trayUseCountdownCheckbox.Name = "trayUseCountdownCheckbox";
            this.trayUseCountdownCheckbox.Size = new System.Drawing.Size(15, 14);
            this.trayUseCountdownCheckbox.TabIndex = 13;
            this.trayUseCountdownCheckbox.UseVisualStyleBackColor = true;
            this.trayUseCountdownCheckbox.CheckedChanged += new System.EventHandler(this.trayUseCountdownCheckbox_CheckedChanged);
            // 
            // trayMinutesComboBox
            // 
            this.trayMinutesComboBox.FormattingEnabled = true;
            this.trayMinutesComboBox.Location = new System.Drawing.Point(226, 255);
            this.trayMinutesComboBox.Name = "trayMinutesComboBox";
            this.trayMinutesComboBox.Size = new System.Drawing.Size(102, 21);
            this.trayMinutesComboBox.TabIndex = 14;
            this.trayMinutesComboBox.Visible = false;
            this.trayMinutesComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // traySelectedColorLabel
            // 
            this.traySelectedColorLabel.AutoSize = true;
            this.traySelectedColorLabel.Location = new System.Drawing.Point(179, 176);
            this.traySelectedColorLabel.Name = "traySelectedColorLabel";
            this.traySelectedColorLabel.Size = new System.Drawing.Size(78, 13);
            this.traySelectedColorLabel.TabIndex = 15;
            this.traySelectedColorLabel.Text = "Selected color:";
            // 
            // trayUseTimerCheckbox
            // 
            this.trayUseTimerCheckbox.AutoSize = true;
            this.trayUseTimerCheckbox.Location = new System.Drawing.Point(115, 282);
            this.trayUseTimerCheckbox.Name = "trayUseTimerCheckbox";
            this.trayUseTimerCheckbox.Size = new System.Drawing.Size(15, 14);
            this.trayUseTimerCheckbox.TabIndex = 17;
            this.trayUseTimerCheckbox.UseVisualStyleBackColor = true;
            this.trayUseTimerCheckbox.CheckedChanged += new System.EventHandler(this.trayUseTimerCheckbox_CheckedChanged);
            // 
            // trayUseTimerLabel
            // 
            this.trayUseTimerLabel.AutoSize = true;
            this.trayUseTimerLabel.ForeColor = System.Drawing.Color.MediumBlue;
            this.trayUseTimerLabel.Location = new System.Drawing.Point(12, 282);
            this.trayUseTimerLabel.Name = "trayUseTimerLabel";
            this.trayUseTimerLabel.Size = new System.Drawing.Size(58, 13);
            this.trayUseTimerLabel.TabIndex = 16;
            this.trayUseTimerLabel.Text = "Use Timer:";
            // 
            // trayAutoAnimateLabel
            // 
            this.trayAutoAnimateLabel.AutoSize = true;
            this.trayAutoAnimateLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.trayAutoAnimateLabel.Location = new System.Drawing.Point(12, 306);
            this.trayAutoAnimateLabel.Name = "trayAutoAnimateLabel";
            this.trayAutoAnimateLabel.Size = new System.Drawing.Size(72, 13);
            this.trayAutoAnimateLabel.TabIndex = 18;
            this.trayAutoAnimateLabel.Text = "Auto animate:";
            // 
            // trayAutoAnimateCheckbox
            // 
            this.trayAutoAnimateCheckbox.AutoSize = true;
            this.trayAutoAnimateCheckbox.Location = new System.Drawing.Point(115, 306);
            this.trayAutoAnimateCheckbox.Name = "trayAutoAnimateCheckbox";
            this.trayAutoAnimateCheckbox.Size = new System.Drawing.Size(15, 14);
            this.trayAutoAnimateCheckbox.TabIndex = 19;
            this.trayAutoAnimateCheckbox.UseVisualStyleBackColor = true;
            this.trayAutoAnimateCheckbox.CheckedChanged += new System.EventHandler(this.trayAutoAnimateCheckbox_CheckedChanged);
            // 
            // trayIntervalLabel
            // 
            this.trayIntervalLabel.AutoSize = true;
            this.trayIntervalLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.trayIntervalLabel.Location = new System.Drawing.Point(137, 306);
            this.trayIntervalLabel.Name = "trayIntervalLabel";
            this.trayIntervalLabel.Size = new System.Drawing.Size(78, 13);
            this.trayIntervalLabel.TabIndex = 20;
            this.trayIntervalLabel.Text = "Interval FadeIn";
            this.trayIntervalLabel.Visible = false;
            // 
            // trayAutoAnimateIntervalFromTextbox
            // 
            this.trayAutoAnimateIntervalFromTextbox.Location = new System.Drawing.Point(215, 303);
            this.trayAutoAnimateIntervalFromTextbox.Name = "trayAutoAnimateIntervalFromTextbox";
            this.trayAutoAnimateIntervalFromTextbox.Size = new System.Drawing.Size(42, 20);
            this.trayAutoAnimateIntervalFromTextbox.TabIndex = 21;
            this.trayAutoAnimateIntervalFromTextbox.Visible = false;
            this.trayAutoAnimateIntervalFromTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // trayAutoAnimateToLabel
            // 
            this.trayAutoAnimateToLabel.AutoSize = true;
            this.trayAutoAnimateToLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.trayAutoAnimateToLabel.Location = new System.Drawing.Point(260, 306);
            this.trayAutoAnimateToLabel.Name = "trayAutoAnimateToLabel";
            this.trayAutoAnimateToLabel.Size = new System.Drawing.Size(23, 13);
            this.trayAutoAnimateToLabel.TabIndex = 22;
            this.trayAutoAnimateToLabel.Text = "To:";
            this.trayAutoAnimateToLabel.Visible = false;
            // 
            // trayAutoAnimateIntervalToTextbox
            // 
            this.trayAutoAnimateIntervalToTextbox.Location = new System.Drawing.Point(286, 303);
            this.trayAutoAnimateIntervalToTextbox.Name = "trayAutoAnimateIntervalToTextbox";
            this.trayAutoAnimateIntervalToTextbox.Size = new System.Drawing.Size(42, 20);
            this.trayAutoAnimateIntervalToTextbox.TabIndex = 23;
            this.trayAutoAnimateIntervalToTextbox.Visible = false;
            this.trayAutoAnimateIntervalToTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // trayCountdownMinutesLabel
            // 
            this.trayCountdownMinutesLabel.AutoSize = true;
            this.trayCountdownMinutesLabel.Location = new System.Drawing.Point(177, 259);
            this.trayCountdownMinutesLabel.Name = "trayCountdownMinutesLabel";
            this.trayCountdownMinutesLabel.Size = new System.Drawing.Size(47, 13);
            this.trayCountdownMinutesLabel.TabIndex = 24;
            this.trayCountdownMinutesLabel.Text = "Minutes:";
            this.trayCountdownMinutesLabel.Visible = false;
            // 
            // traySaveAndCloseButton
            // 
            this.traySaveAndCloseButton.Location = new System.Drawing.Point(15, 379);
            this.traySaveAndCloseButton.Name = "traySaveAndCloseButton";
            this.traySaveAndCloseButton.Size = new System.Drawing.Size(128, 23);
            this.traySaveAndCloseButton.TabIndex = 25;
            this.traySaveAndCloseButton.Text = "Save And Close";
            this.traySaveAndCloseButton.UseVisualStyleBackColor = true;
            this.traySaveAndCloseButton.Click += new System.EventHandler(this.traySaveAndCloseButton_Click);
            // 
            // trayGraphicsDialogBox
            // 
            this.trayGraphicsDialogBox.FileName = "openFileDialog1";
            this.trayGraphicsDialogBox.Filter = "Graphic files |*.png;*.jpg";
            // 
            // trayCloseWithoutSaveButton
            // 
            this.trayCloseWithoutSaveButton.Location = new System.Drawing.Point(16, 379);
            this.trayCloseWithoutSaveButton.Name = "trayCloseWithoutSaveButton";
            this.trayCloseWithoutSaveButton.Size = new System.Drawing.Size(127, 23);
            this.trayCloseWithoutSaveButton.TabIndex = 26;
            this.trayCloseWithoutSaveButton.Text = "Close Without Save";
            this.trayCloseWithoutSaveButton.UseVisualStyleBackColor = true;
            this.trayCloseWithoutSaveButton.Click += new System.EventHandler(this.trayCloseWithoutSaveButton_Click);
            // 
            // trayFilenameLabel
            // 
            this.trayFilenameLabel.AutoSize = true;
            this.trayFilenameLabel.Location = new System.Drawing.Point(148, 28);
            this.trayFilenameLabel.Name = "trayFilenameLabel";
            this.trayFilenameLabel.Size = new System.Drawing.Size(52, 13);
            this.trayFilenameLabel.TabIndex = 27;
            this.trayFilenameLabel.Text = "Filename:";
            // 
            // trayFileNameLabelText
            // 
            this.trayFileNameLabelText.AutoSize = true;
            this.trayFileNameLabelText.Location = new System.Drawing.Point(206, 28);
            this.trayFileNameLabelText.Name = "trayFileNameLabelText";
            this.trayFileNameLabelText.Size = new System.Drawing.Size(0, 13);
            this.trayFileNameLabelText.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 29;
            // 
            // trayRandomIntervalInfoLabel
            // 
            this.trayRandomIntervalInfoLabel.AutoSize = true;
            this.trayRandomIntervalInfoLabel.Location = new System.Drawing.Point(148, 357);
            this.trayRandomIntervalInfoLabel.Name = "trayRandomIntervalInfoLabel";
            this.trayRandomIntervalInfoLabel.Size = new System.Drawing.Size(184, 39);
            this.trayRandomIntervalInfoLabel.TabIndex = 30;
            this.trayRandomIntervalInfoLabel.Text = "Random from x seconds to y seconds\r\nFade In =Off  screen time\r\nFade Out = On scre" +
    "en time";
            // 
            // trayAutoAnimateFadeOutToTextbox
            // 
            this.trayAutoAnimateFadeOutToTextbox.Location = new System.Drawing.Point(286, 334);
            this.trayAutoAnimateFadeOutToTextbox.Name = "trayAutoAnimateFadeOutToTextbox";
            this.trayAutoAnimateFadeOutToTextbox.Size = new System.Drawing.Size(42, 20);
            this.trayAutoAnimateFadeOutToTextbox.TabIndex = 34;
            this.trayAutoAnimateFadeOutToTextbox.Visible = false;
            this.trayAutoAnimateFadeOutToTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // trayFadeoutToLabel
            // 
            this.trayFadeoutToLabel.AutoSize = true;
            this.trayFadeoutToLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.trayFadeoutToLabel.Location = new System.Drawing.Point(260, 337);
            this.trayFadeoutToLabel.Name = "trayFadeoutToLabel";
            this.trayFadeoutToLabel.Size = new System.Drawing.Size(23, 13);
            this.trayFadeoutToLabel.TabIndex = 33;
            this.trayFadeoutToLabel.Text = "To:";
            this.trayFadeoutToLabel.Visible = false;
            // 
            // trayAutoAnimateFadeOutFromTextbox
            // 
            this.trayAutoAnimateFadeOutFromTextbox.Location = new System.Drawing.Point(215, 334);
            this.trayAutoAnimateFadeOutFromTextbox.Name = "trayAutoAnimateFadeOutFromTextbox";
            this.trayAutoAnimateFadeOutFromTextbox.Size = new System.Drawing.Size(42, 20);
            this.trayAutoAnimateFadeOutFromTextbox.TabIndex = 32;
            this.trayAutoAnimateFadeOutFromTextbox.Visible = false;
            this.trayAutoAnimateFadeOutFromTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onEnterCharacters);
            // 
            // trayIntervalFadeOutLabel
            // 
            this.trayIntervalFadeOutLabel.AutoSize = true;
            this.trayIntervalFadeOutLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.trayIntervalFadeOutLabel.Location = new System.Drawing.Point(128, 337);
            this.trayIntervalFadeOutLabel.Name = "trayIntervalFadeOutLabel";
            this.trayIntervalFadeOutLabel.Size = new System.Drawing.Size(86, 13);
            this.trayIntervalFadeOutLabel.TabIndex = 31;
            this.trayIntervalFadeOutLabel.Text = "Interval FadeOut";
            this.trayIntervalFadeOutLabel.Visible = false;
            // 
            // AddTrayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 414);
            this.Controls.Add(this.trayAutoAnimateFadeOutToTextbox);
            this.Controls.Add(this.trayFadeoutToLabel);
            this.Controls.Add(this.trayAutoAnimateFadeOutFromTextbox);
            this.Controls.Add(this.trayIntervalFadeOutLabel);
            this.Controls.Add(this.trayRandomIntervalInfoLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trayFileNameLabelText);
            this.Controls.Add(this.trayFilenameLabel);
            this.Controls.Add(this.trayCloseWithoutSaveButton);
            this.Controls.Add(this.traySaveAndCloseButton);
            this.Controls.Add(this.trayCountdownMinutesLabel);
            this.Controls.Add(this.trayAutoAnimateIntervalToTextbox);
            this.Controls.Add(this.trayAutoAnimateToLabel);
            this.Controls.Add(this.trayAutoAnimateIntervalFromTextbox);
            this.Controls.Add(this.trayIntervalLabel);
            this.Controls.Add(this.trayAutoAnimateCheckbox);
            this.Controls.Add(this.trayAutoAnimateLabel);
            this.Controls.Add(this.trayUseTimerCheckbox);
            this.Controls.Add(this.trayUseTimerLabel);
            this.Controls.Add(this.traySelectedColorLabel);
            this.Controls.Add(this.trayMinutesComboBox);
            this.Controls.Add(this.trayUseCountdownCheckbox);
            this.Controls.Add(this.trayUseCountdownLabel);
            this.Controls.Add(this.traySelectColorButton);
            this.Controls.Add(this.trayAnimDirectionLabel);
            this.Controls.Add(this.trayAnimationDirectionCombobox);
            this.Controls.Add(this.trayFadeOutTextbox);
            this.Controls.Add(this.trayFadeOutSpeedLabel);
            this.Controls.Add(this.traySelectedColorPictureBox);
            this.Controls.Add(this.trayTextColorLabel);
            this.Controls.Add(this.trayFadeInTextbox);
            this.Controls.Add(this.trayFadeInSpeedLabel);
            this.Controls.Add(this.trayTextTextbox);
            this.Controls.Add(this.trayTextLabel);
            this.Controls.Add(this.trayAddGraphics);
            this.Name = "AddTrayForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddTrayForm";
            ((System.ComponentModel.ISupportInitialize)(this.traySelectedColorPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button trayAddGraphics;
        private System.Windows.Forms.Label trayTextLabel;
        private System.Windows.Forms.TextBox trayTextTextbox;
        private System.Windows.Forms.Label trayFadeInSpeedLabel;
        private System.Windows.Forms.TextBox trayFadeInTextbox;
        private System.Windows.Forms.Label trayTextColorLabel;
        private System.Windows.Forms.ColorDialog textColorDialog;
        private System.Windows.Forms.PictureBox traySelectedColorPictureBox;
        private System.Windows.Forms.TextBox trayFadeOutTextbox;
        private System.Windows.Forms.Label trayFadeOutSpeedLabel;
        private System.Windows.Forms.ComboBox trayAnimationDirectionCombobox;
        private System.Windows.Forms.Label trayAnimDirectionLabel;
        private System.Windows.Forms.Button traySelectColorButton;
        private System.Windows.Forms.Label trayUseCountdownLabel;
        private System.Windows.Forms.CheckBox trayUseCountdownCheckbox;
        private System.Windows.Forms.ComboBox trayMinutesComboBox;
        private System.Windows.Forms.Label traySelectedColorLabel;
        private System.Windows.Forms.CheckBox trayUseTimerCheckbox;
        private System.Windows.Forms.Label trayUseTimerLabel;
        private System.Windows.Forms.ToolTip addTrayTooltip;
        private System.Windows.Forms.Label trayAutoAnimateLabel;
        private System.Windows.Forms.CheckBox trayAutoAnimateCheckbox;
        private System.Windows.Forms.Label trayIntervalLabel;
        private System.Windows.Forms.TextBox trayAutoAnimateIntervalFromTextbox;
        private System.Windows.Forms.Label trayAutoAnimateToLabel;
        private System.Windows.Forms.TextBox trayAutoAnimateIntervalToTextbox;
        private System.Windows.Forms.Label trayCountdownMinutesLabel;
        private System.Windows.Forms.Button traySaveAndCloseButton;
        private System.Windows.Forms.OpenFileDialog trayGraphicsDialogBox;
        private System.Windows.Forms.Button trayCloseWithoutSaveButton;
        private System.Windows.Forms.Label trayFilenameLabel;
        private System.Windows.Forms.Label trayFileNameLabelText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label trayRandomIntervalInfoLabel;
        private System.Windows.Forms.TextBox trayAutoAnimateFadeOutToTextbox;
        private System.Windows.Forms.Label trayFadeoutToLabel;
        private System.Windows.Forms.TextBox trayAutoAnimateFadeOutFromTextbox;
        private System.Windows.Forms.Label trayIntervalFadeOutLabel;
    }
}