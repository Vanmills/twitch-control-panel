﻿namespace StreamOverlayControlPanel
{
    partial class AnimationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.quickSettingsLabel = new System.Windows.Forms.Label();
            this.quickSettingsCombobox = new System.Windows.Forms.ComboBox();
            this.animFadeInLabel = new System.Windows.Forms.Label();
            this.animFadeInCheckbox = new System.Windows.Forms.CheckBox();
            this.fadeInDurationTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rotateInCheckbox = new System.Windows.Forms.CheckBox();
            this.animFadeOutCheckbox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rotateInDegreesTextbox = new System.Windows.Forms.TextBox();
            this.zoomOutCheckbox = new System.Windows.Forms.CheckBox();
            this.zoomInSizeTextbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.zoomInCheckbox = new System.Windows.Forms.CheckBox();
            this.rotateInClockwiseCheckbox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.zoomInSpeedTextbox = new System.Windows.Forms.TextBox();
            this.zoomOutSpeedTextbox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.selectFileToAnimate = new System.Windows.Forms.Button();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fadeOutDurationTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rotateInStartTextbox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.rotateInDurationTextbox = new System.Windows.Forms.TextBox();
            this.rotateOutDurationTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rotateOutStartTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rotateOutClocwiseCheckbox = new System.Windows.Forms.CheckBox();
            this.rotateOutDegreesTextbox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.rotateOutCheckbox = new System.Windows.Forms.CheckBox();
            this.zoomOutSizeTextbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.zoomOutStartTimeTextbox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.zoomInStartTimeTextbox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.saveAndCloseButton = new System.Windows.Forms.Button();
            this.testAnimationButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.fadeOutTimeTextbox = new System.Windows.Forms.TextBox();
            this.animateFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.totalAnimationDurationLabel = new System.Windows.Forms.Label();
            this.totalAnimationDurationTextbox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // quickSettingsLabel
            // 
            this.quickSettingsLabel.AutoSize = true;
            this.quickSettingsLabel.Location = new System.Drawing.Point(22, 64);
            this.quickSettingsLabel.Name = "quickSettingsLabel";
            this.quickSettingsLabel.Size = new System.Drawing.Size(76, 13);
            this.quickSettingsLabel.TabIndex = 1;
            this.quickSettingsLabel.Text = "Quick Settings";
            // 
            // quickSettingsCombobox
            // 
            this.quickSettingsCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.quickSettingsCombobox.FormattingEnabled = true;
            this.quickSettingsCombobox.Items.AddRange(new object[] {
            "Default -ZoomInOut, FadeIn Out, Rotate",
            "Zoom - RotateOut, Fade Out",
            "ZoomIn, RotateIn, Zoom Out",
            "RotateIn, RotateOut, Zoom 0ut"});
            this.quickSettingsCombobox.Location = new System.Drawing.Point(126, 61);
            this.quickSettingsCombobox.Name = "quickSettingsCombobox";
            this.quickSettingsCombobox.Size = new System.Drawing.Size(213, 21);
            this.quickSettingsCombobox.TabIndex = 1;
            this.quickSettingsCombobox.SelectedIndexChanged += new System.EventHandler(this.quickSettingsCombobox_SelectedIndexChanged);
            // 
            // animFadeInLabel
            // 
            this.animFadeInLabel.AutoSize = true;
            this.animFadeInLabel.Location = new System.Drawing.Point(22, 126);
            this.animFadeInLabel.Name = "animFadeInLabel";
            this.animFadeInLabel.Size = new System.Drawing.Size(115, 13);
            this.animFadeInLabel.TabIndex = 3;
            this.animFadeInLabel.Text = "Fade In / Out Settings:";
            // 
            // animFadeInCheckbox
            // 
            this.animFadeInCheckbox.AutoSize = true;
            this.animFadeInCheckbox.Location = new System.Drawing.Point(25, 151);
            this.animFadeInCheckbox.Name = "animFadeInCheckbox";
            this.animFadeInCheckbox.Size = new System.Drawing.Size(111, 17);
            this.animFadeInCheckbox.TabIndex = 2;
            this.animFadeInCheckbox.Text = "Fade In Animation";
            this.animFadeInCheckbox.UseVisualStyleBackColor = true;
            // 
            // fadeInDurationTextbox
            // 
            this.fadeInDurationTextbox.Location = new System.Drawing.Point(325, 145);
            this.fadeInDurationTextbox.Name = "fadeInDurationTextbox";
            this.fadeInDurationTextbox.Size = new System.Drawing.Size(66, 20);
            this.fadeInDurationTextbox.TabIndex = 3;
            this.fadeInDurationTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Fade In Duration(Seconds):";
            // 
            // rotateInCheckbox
            // 
            this.rotateInCheckbox.AutoSize = true;
            this.rotateInCheckbox.Location = new System.Drawing.Point(25, 269);
            this.rotateInCheckbox.Name = "rotateInCheckbox";
            this.rotateInCheckbox.Size = new System.Drawing.Size(119, 17);
            this.rotateInCheckbox.TabIndex = 7;
            this.rotateInCheckbox.Text = "Rotate In Animation";
            this.rotateInCheckbox.UseVisualStyleBackColor = true;
            // 
            // animFadeOutCheckbox
            // 
            this.animFadeOutCheckbox.AutoSize = true;
            this.animFadeOutCheckbox.Location = new System.Drawing.Point(25, 183);
            this.animFadeOutCheckbox.Name = "animFadeOutCheckbox";
            this.animFadeOutCheckbox.Size = new System.Drawing.Size(119, 17);
            this.animFadeOutCheckbox.TabIndex = 4;
            this.animFadeOutCheckbox.Text = "Fade Out Animation";
            this.animFadeOutCheckbox.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(285, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Total degrees to rotate:";
            // 
            // rotateInDegreesTextbox
            // 
            this.rotateInDegreesTextbox.Location = new System.Drawing.Point(428, 264);
            this.rotateInDegreesTextbox.Name = "rotateInDegreesTextbox";
            this.rotateInDegreesTextbox.Size = new System.Drawing.Size(66, 20);
            this.rotateInDegreesTextbox.TabIndex = 9;
            this.rotateInDegreesTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // zoomOutCheckbox
            // 
            this.zoomOutCheckbox.AutoSize = true;
            this.zoomOutCheckbox.Location = new System.Drawing.Point(25, 431);
            this.zoomOutCheckbox.Name = "zoomOutCheckbox";
            this.zoomOutCheckbox.Size = new System.Drawing.Size(73, 17);
            this.zoomOutCheckbox.TabIndex = 21;
            this.zoomOutCheckbox.Text = "Zoom Out";
            this.zoomOutCheckbox.UseVisualStyleBackColor = true;
            // 
            // zoomInSizeTextbox
            // 
            this.zoomInSizeTextbox.Location = new System.Drawing.Point(159, 398);
            this.zoomInSizeTextbox.Name = "zoomInSizeTextbox";
            this.zoomInSizeTextbox.Size = new System.Drawing.Size(66, 20);
            this.zoomInSizeTextbox.TabIndex = 18;
            this.zoomInSizeTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(118, 401);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Size %:";
            // 
            // zoomInCheckbox
            // 
            this.zoomInCheckbox.AutoSize = true;
            this.zoomInCheckbox.Location = new System.Drawing.Point(25, 400);
            this.zoomInCheckbox.Name = "zoomInCheckbox";
            this.zoomInCheckbox.Size = new System.Drawing.Size(65, 17);
            this.zoomInCheckbox.TabIndex = 17;
            this.zoomInCheckbox.Text = "Zoom In";
            this.zoomInCheckbox.UseVisualStyleBackColor = true;
            // 
            // rotateInClockwiseCheckbox
            // 
            this.rotateInClockwiseCheckbox.AutoSize = true;
            this.rotateInClockwiseCheckbox.Location = new System.Drawing.Point(159, 269);
            this.rotateInClockwiseCheckbox.Name = "rotateInClockwiseCheckbox";
            this.rotateInClockwiseCheckbox.Size = new System.Drawing.Size(122, 17);
            this.rotateInClockwiseCheckbox.TabIndex = 8;
            this.rotateInClockwiseCheckbox.Text = "Counter-CW rotation";
            this.rotateInClockwiseCheckbox.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(236, 403);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Speed";
            // 
            // zoomInSpeedTextbox
            // 
            this.zoomInSpeedTextbox.Location = new System.Drawing.Point(273, 400);
            this.zoomInSpeedTextbox.Name = "zoomInSpeedTextbox";
            this.zoomInSpeedTextbox.Size = new System.Drawing.Size(66, 20);
            this.zoomInSpeedTextbox.TabIndex = 19;
            this.zoomInSpeedTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // zoomOutSpeedTextbox
            // 
            this.zoomOutSpeedTextbox.Location = new System.Drawing.Point(273, 429);
            this.zoomOutSpeedTextbox.Name = "zoomOutSpeedTextbox";
            this.zoomOutSpeedTextbox.Size = new System.Drawing.Size(66, 20);
            this.zoomOutSpeedTextbox.TabIndex = 23;
            this.zoomOutSpeedTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(236, 432);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Speed";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "Select Graphics to animate:";
            // 
            // selectFileToAnimate
            // 
            this.selectFileToAnimate.Location = new System.Drawing.Point(171, 14);
            this.selectFileToAnimate.Name = "selectFileToAnimate";
            this.selectFileToAnimate.Size = new System.Drawing.Size(103, 23);
            this.selectFileToAnimate.TabIndex = 0;
            this.selectFileToAnimate.Text = "Open File Dialog";
            this.selectFileToAnimate.UseVisualStyleBackColor = true;
            this.selectFileToAnimate.Click += new System.EventHandler(this.selectFileToAnimate_Click);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Location = new System.Drawing.Point(22, 42);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(55, 13);
            this.fileNameLabel.TabIndex = 37;
            this.fileNameLabel.Text = "Filename: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Fade Out Duration(Seconds):";
            // 
            // fadeOutDurationTextbox
            // 
            this.fadeOutDurationTextbox.Location = new System.Drawing.Point(325, 183);
            this.fadeOutDurationTextbox.Name = "fadeOutDurationTextbox";
            this.fadeOutDurationTextbox.Size = new System.Drawing.Size(66, 20);
            this.fadeOutDurationTextbox.TabIndex = 5;
            this.fadeOutDurationTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Rotate In/Out Settings";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 299);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Start rotation after seconds:";
            // 
            // rotateInStartTextbox
            // 
            this.rotateInStartTextbox.Location = new System.Drawing.Point(184, 296);
            this.rotateInStartTextbox.Name = "rotateInStartTextbox";
            this.rotateInStartTextbox.Size = new System.Drawing.Size(100, 20);
            this.rotateInStartTextbox.TabIndex = 10;
            this.rotateInStartTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(290, 299);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Duration of rotation:";
            // 
            // rotateInDurationTextbox
            // 
            this.rotateInDurationTextbox.Location = new System.Drawing.Point(428, 296);
            this.rotateInDurationTextbox.Name = "rotateInDurationTextbox";
            this.rotateInDurationTextbox.Size = new System.Drawing.Size(66, 20);
            this.rotateInDurationTextbox.TabIndex = 11;
            this.rotateInDurationTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // rotateOutDurationTextbox
            // 
            this.rotateOutDurationTextbox.Location = new System.Drawing.Point(428, 359);
            this.rotateOutDurationTextbox.Name = "rotateOutDurationTextbox";
            this.rotateOutDurationTextbox.Size = new System.Drawing.Size(66, 20);
            this.rotateOutDurationTextbox.TabIndex = 16;
            this.rotateOutDurationTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 362);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 53;
            this.label6.Text = "Duration of rotation:";
            // 
            // rotateOutStartTextbox
            // 
            this.rotateOutStartTextbox.Location = new System.Drawing.Point(181, 359);
            this.rotateOutStartTextbox.Name = "rotateOutStartTextbox";
            this.rotateOutStartTextbox.Size = new System.Drawing.Size(100, 20);
            this.rotateOutStartTextbox.TabIndex = 15;
            this.rotateOutStartTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 362);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 13);
            this.label7.TabIndex = 51;
            this.label7.Text = "Start rotation after seconds:";
            // 
            // rotateOutClocwiseCheckbox
            // 
            this.rotateOutClocwiseCheckbox.AutoSize = true;
            this.rotateOutClocwiseCheckbox.Location = new System.Drawing.Point(156, 332);
            this.rotateOutClocwiseCheckbox.Name = "rotateOutClocwiseCheckbox";
            this.rotateOutClocwiseCheckbox.Size = new System.Drawing.Size(122, 17);
            this.rotateOutClocwiseCheckbox.TabIndex = 13;
            this.rotateOutClocwiseCheckbox.Text = "Counter-CW rotation";
            this.rotateOutClocwiseCheckbox.UseVisualStyleBackColor = true;
            // 
            // rotateOutDegreesTextbox
            // 
            this.rotateOutDegreesTextbox.Location = new System.Drawing.Point(428, 327);
            this.rotateOutDegreesTextbox.Name = "rotateOutDegreesTextbox";
            this.rotateOutDegreesTextbox.Size = new System.Drawing.Size(66, 20);
            this.rotateOutDegreesTextbox.TabIndex = 14;
            this.rotateOutDegreesTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(282, 333);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 13);
            this.label16.TabIndex = 48;
            this.label16.Text = "Total degrees to rotate:";
            // 
            // rotateOutCheckbox
            // 
            this.rotateOutCheckbox.AutoSize = true;
            this.rotateOutCheckbox.Location = new System.Drawing.Point(25, 327);
            this.rotateOutCheckbox.Name = "rotateOutCheckbox";
            this.rotateOutCheckbox.Size = new System.Drawing.Size(127, 17);
            this.rotateOutCheckbox.TabIndex = 12;
            this.rotateOutCheckbox.Text = "Rotate Out Animation";
            this.rotateOutCheckbox.UseVisualStyleBackColor = true;
            // 
            // zoomOutSizeTextbox
            // 
            this.zoomOutSizeTextbox.Location = new System.Drawing.Point(159, 428);
            this.zoomOutSizeTextbox.Name = "zoomOutSizeTextbox";
            this.zoomOutSizeTextbox.Size = new System.Drawing.Size(66, 20);
            this.zoomOutSizeTextbox.TabIndex = 22;
            this.zoomOutSizeTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(118, 432);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 55;
            this.label17.Text = "Size %:";
            // 
            // zoomOutStartTimeTextbox
            // 
            this.zoomOutStartTimeTextbox.Location = new System.Drawing.Point(429, 428);
            this.zoomOutStartTimeTextbox.Name = "zoomOutStartTimeTextbox";
            this.zoomOutStartTimeTextbox.Size = new System.Drawing.Size(66, 20);
            this.zoomOutStartTimeTextbox.TabIndex = 24;
            this.zoomOutStartTimeTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(351, 432);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "Time To Start";
            // 
            // zoomInStartTimeTextbox
            // 
            this.zoomInStartTimeTextbox.Location = new System.Drawing.Point(428, 397);
            this.zoomInStartTimeTextbox.Name = "zoomInStartTimeTextbox";
            this.zoomInStartTimeTextbox.Size = new System.Drawing.Size(66, 20);
            this.zoomInStartTimeTextbox.TabIndex = 20;
            this.zoomInStartTimeTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(351, 403);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 13);
            this.label19.TabIndex = 57;
            this.label19.Text = "Time To Start";
            // 
            // saveAndCloseButton
            // 
            this.saveAndCloseButton.Location = new System.Drawing.Point(25, 476);
            this.saveAndCloseButton.Name = "saveAndCloseButton";
            this.saveAndCloseButton.Size = new System.Drawing.Size(100, 23);
            this.saveAndCloseButton.TabIndex = 26;
            this.saveAndCloseButton.Text = "Save and Close";
            this.saveAndCloseButton.UseVisualStyleBackColor = true;
            this.saveAndCloseButton.Click += new System.EventHandler(this.saveAndCloseButton_Click);
            // 
            // testAnimationButton
            // 
            this.testAnimationButton.Location = new System.Drawing.Point(358, 461);
            this.testAnimationButton.Name = "testAnimationButton";
            this.testAnimationButton.Size = new System.Drawing.Size(122, 23);
            this.testAnimationButton.TabIndex = 25;
            this.testAnimationButton.Text = "Test Animation";
            this.testAnimationButton.UseVisualStyleBackColor = true;
            this.testAnimationButton.Click += new System.EventHandler(this.testAnimationButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 209);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "Fade Out after seconds:";
            // 
            // fadeOutTimeTextbox
            // 
            this.fadeOutTimeTextbox.Location = new System.Drawing.Point(156, 206);
            this.fadeOutTimeTextbox.Name = "fadeOutTimeTextbox";
            this.fadeOutTimeTextbox.Size = new System.Drawing.Size(37, 20);
            this.fadeOutTimeTextbox.TabIndex = 6;
            this.fadeOutTimeTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // animateFileDialog
            // 
            this.animateFileDialog.FileName = "openFileDialog1";
            this.animateFileDialog.Filter = "Graphic files |*.png;*.jpg";
            // 
            // totalAnimationDurationLabel
            // 
            this.totalAnimationDurationLabel.AutoSize = true;
            this.totalAnimationDurationLabel.Location = new System.Drawing.Point(20, 92);
            this.totalAnimationDurationLabel.Name = "totalAnimationDurationLabel";
            this.totalAnimationDurationLabel.Size = new System.Drawing.Size(123, 13);
            this.totalAnimationDurationLabel.TabIndex = 64;
            this.totalAnimationDurationLabel.Text = "Total animation duration:";
            // 
            // totalAnimationDurationTextbox
            // 
            this.totalAnimationDurationTextbox.Location = new System.Drawing.Point(158, 89);
            this.totalAnimationDurationTextbox.Name = "totalAnimationDurationTextbox";
            this.totalAnimationDurationTextbox.Size = new System.Drawing.Size(46, 20);
            this.totalAnimationDurationTextbox.TabIndex = 65;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Maroon;
            this.label11.Location = new System.Drawing.Point(49, 382);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(326, 13);
            this.label11.TabIndex = 66;
            this.label11.Text = "Note: Size % is based on the image size, not the size of the window.";
            // 
            // AnimationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 511);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.totalAnimationDurationTextbox);
            this.Controls.Add(this.totalAnimationDurationLabel);
            this.Controls.Add(this.fadeOutTimeTextbox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.testAnimationButton);
            this.Controls.Add(this.saveAndCloseButton);
            this.Controls.Add(this.zoomOutStartTimeTextbox);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.zoomInStartTimeTextbox);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.zoomOutSizeTextbox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.rotateOutDurationTextbox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rotateOutStartTextbox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rotateOutClocwiseCheckbox);
            this.Controls.Add(this.rotateOutDegreesTextbox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.rotateOutCheckbox);
            this.Controls.Add(this.rotateInDurationTextbox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.rotateInStartTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fadeOutDurationTextbox);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.selectFileToAnimate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.zoomOutSpeedTextbox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.zoomInSpeedTextbox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.rotateInClockwiseCheckbox);
            this.Controls.Add(this.zoomOutCheckbox);
            this.Controls.Add(this.zoomInSizeTextbox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.zoomInCheckbox);
            this.Controls.Add(this.rotateInDegreesTextbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.animFadeOutCheckbox);
            this.Controls.Add(this.rotateInCheckbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fadeInDurationTextbox);
            this.Controls.Add(this.animFadeInCheckbox);
            this.Controls.Add(this.animFadeInLabel);
            this.Controls.Add(this.quickSettingsCombobox);
            this.Controls.Add(this.quickSettingsLabel);
            this.Name = "AnimationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AnimationForm";
            this.Load += new System.EventHandler(this.AnimationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label quickSettingsLabel;
        private System.Windows.Forms.ComboBox quickSettingsCombobox;
        private System.Windows.Forms.Label animFadeInLabel;
        private System.Windows.Forms.CheckBox animFadeInCheckbox;
        private System.Windows.Forms.TextBox fadeInDurationTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox rotateInCheckbox;
        private System.Windows.Forms.CheckBox animFadeOutCheckbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox rotateInDegreesTextbox;
        private System.Windows.Forms.CheckBox zoomOutCheckbox;
        private System.Windows.Forms.TextBox zoomInSizeTextbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox zoomInCheckbox;
        private System.Windows.Forms.CheckBox rotateInClockwiseCheckbox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox zoomInSpeedTextbox;
        private System.Windows.Forms.TextBox zoomOutSpeedTextbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button selectFileToAnimate;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fadeOutDurationTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox rotateInStartTextbox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox rotateInDurationTextbox;
        private System.Windows.Forms.TextBox rotateOutDurationTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox rotateOutStartTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox rotateOutClocwiseCheckbox;
        private System.Windows.Forms.TextBox rotateOutDegreesTextbox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox rotateOutCheckbox;
        private System.Windows.Forms.TextBox zoomOutSizeTextbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox zoomOutStartTimeTextbox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox zoomInStartTimeTextbox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button saveAndCloseButton;
        private System.Windows.Forms.Button testAnimationButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox fadeOutTimeTextbox;
        private System.Windows.Forms.OpenFileDialog animateFileDialog;
        private System.Windows.Forms.Label totalAnimationDurationLabel;
        private System.Windows.Forms.TextBox totalAnimationDurationTextbox;
        private System.Windows.Forms.Label label11;
    }
}