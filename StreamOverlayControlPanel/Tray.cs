﻿using System;
using System.Drawing;
using System.IO;

public enum TrayDirection
{
    Up = 0,
    Down,
    Left,
    Right
}

namespace StreamOverlayControlPanel
{
    [Serializable]
    public class Tray
    {
        public int trayID = -1;
        public string trayFilename = "";
        public string trayText = "";
        public Color trayTextColor = Color.White;
        public bool trayUseCountdown = false;
        public int trayFadeInSpeed = 20;
        public int trayFadeOutSpeed = 20;
        public TrayDirection trayDirection = TrayDirection.Right;
        public int trayCountdownMinutes = 60;
        public bool trayUseTimer = false;
        public bool trayUseAutoAnimate = false;
        public int trayFadeInIntervalFromSeconds = 360;
        public int trayFadeInIntervalToSeconds = 360;
        public int trayFadeOutIntervalFromSeconds = 60;
        public int trayFadeOutIntervalToSeconds = 60;

        public override string ToString()
        {
            return "ID: " + trayID + 
                   " Filename: " + trayFilename +
                   " trayText: " + trayText +
                   " trayTextColor: " + trayTextColor.R + " " + trayTextColor.G + " " + trayTextColor.B +
                   " FadeIn: " + trayFadeInSpeed + " fadeout: " + trayFadeOutSpeed +
                   " traydirection: " + trayDirection +
                   " traycountdown: " + trayUseCountdown + " minutes: " + trayCountdownMinutes +
                   " trayTimer: " + trayUseTimer +
                   " trayAutoAnimate: " + trayUseAutoAnimate +
                   " trayInterval FadeIn From: " + trayFadeInIntervalFromSeconds + " to: " + trayFadeInIntervalToSeconds +
                   " trayInterval Fadeout From: " + trayFadeOutIntervalFromSeconds + " to: " + trayFadeOutIntervalToSeconds;
        }

        //public string GetKey(TrayKeys key)
        //{
        //    switch (key)
        //    {
        //        case TrayKeys.Identifier:
        //            return "<ID:" + trayID + ">";
        //            //break;
        //        case TrayKeys.Filename:
        //            return "<Filename:" + trayFilename + ">";
        //        case TrayKeys.Text:
        //            return "<Text:" + trayText + ">";
        //        case TrayKeys.TextColor:
        //            return "<TextColor: R(" + trayTextColor.R + ")G(" + trayTextColor.G + ")B(" + trayTextColor.B + ")>";
        //        case TrayKeys.FadeIn:
        //            return "<FadeIn:" + trayFadeInSpeed + ">";
        //        case TrayKeys.FadeOut:
        //            return "<FadeOut:" + trayFadeOutSpeed + ">";
        //        case TrayKeys.Direction:
        //            return "<Direction:" + trayDirection.ToString() + ">";
        //        case TrayKeys.CountdownBool:
        //            return "<UseCooldown:" + trayUseCountdown + ">";
        //        case TrayKeys.CountdownTime:
        //            return "<Countdown:" + trayCountdownMinutes + ">";
        //        case TrayKeys.UseTimer:
        //            return "<UseTimer:" + trayUseTimer + ">";
        //        case TrayKeys.AutoAnimateBool:
        //            return "<UseAutoAnimate:" + trayUseAutoAnimate + ">";
        //        case TrayKeys.AutoFadeInAnimateFrom:
        //            return "<FadeInIntervalFrom:" + trayFadeInIntervalFromSeconds + ">";
        //        case TrayKeys.AutoFadeInAnimateTo:
        //            return "<FadeInIntervalTo: " + trayFadeInIntervalToSeconds + ">";
        //        case TrayKeys.AutoFadeOutAnimateFrom:
        //            return "<FadeOutIntervalFrom:" + trayFadeOutIntervalFromSeconds + ">";
        //        case TrayKeys.AutoFadeOutAnimateTo:
        //            return "<FadeOutIntervalTo:" + trayFadeOutIntervalToSeconds + ">";
        //        case TrayKeys.All:

        //            return "<ID:" + trayID + ">" +
        //                   "<Filename:" + trayFilename + ">" +
        //                   "<Text:" + trayText + ">" +
        //                   "<TextColor: R(" + trayTextColor.R + ")G(" + trayTextColor.G + ")B(" + trayTextColor.B + ")>" +
        //                   "<FadeIn:" + trayFadeInSpeed + ">" +
        //                   "<FadeOut:" + trayFadeOutSpeed + ">" +
        //                   "<Direction:" + trayDirection.ToString() + ">" +
        //                   "<UseCountdown:" + trayUseCountdown + ">" +
        //                   "<Countdown:" + trayCountdownMinutes + ">" +
        //                   "<UseTimer:" + trayUseTimer + ">" +
        //                   "<UseAutoAnimate:" + trayUseAutoAnimate + ">" +
        //                   "<FadeInIntervalFrom:" + trayFadeInIntervalFromSeconds + ">" +
        //                   "<FadeInIntervalTo: " + trayFadeInIntervalToSeconds + ">" +
        //                   "<FadeOutIntervalFrom:" + trayFadeOutIntervalFromSeconds + ">" +
        //                   "<FadeOutIntervalTo: " + trayFadeOutIntervalToSeconds + ">";
        //        default:
        //            return "Not a valid key";

        //    }

        //}

        public static byte[] SerializeClass(Tray pTray)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);
            writer.Write(pTray.trayID);
            writer.Write(pTray.trayFilename);
            writer.Write(pTray.trayText);
            writer.Write(pTray.trayTextColor.R);
            writer.Write(pTray.trayTextColor.G);
            writer.Write(pTray.trayTextColor.B);
            writer.Write(pTray.trayFadeInSpeed);
            writer.Write(pTray.trayFadeOutSpeed);
            writer.Write((int)pTray.trayDirection);
            writer.Write(pTray.trayUseCountdown);
            writer.Write(pTray.trayCountdownMinutes);
            writer.Write(pTray.trayUseTimer);
            writer.Write(pTray.trayUseAutoAnimate);
            writer.Write(pTray.trayFadeInIntervalFromSeconds);
            writer.Write(pTray.trayFadeInIntervalToSeconds);
            writer.Write(pTray.trayFadeOutIntervalFromSeconds);
            writer.Write(pTray.trayFadeOutIntervalToSeconds);
            return ms.ToArray();
        }


        public static Tray DeserializeClass(byte[] buffer)
        {
            MemoryStream memStream = new MemoryStream(buffer);
            BinaryReader reader = new BinaryReader(memStream);
            Tray tray = new Tray();
            byte r;
            byte g;
            byte b;

            tray.trayID = reader.ReadInt32();
            tray.trayFilename = reader.ReadString();
            tray.trayText = reader.ReadString();

            r = reader.ReadByte();
            g = reader.ReadByte();
            b = reader.ReadByte();

            tray.trayFadeInSpeed = reader.ReadInt32();
            tray.trayFadeOutSpeed = reader.ReadInt32();
            tray.trayDirection = (TrayDirection) reader.ReadInt32();
            tray.trayUseCountdown = reader.ReadBoolean();
            tray.trayCountdownMinutes = reader.ReadInt32();
            tray.trayUseTimer = reader.ReadBoolean();
            tray.trayUseAutoAnimate = reader.ReadBoolean();
            tray.trayFadeInIntervalFromSeconds = reader.ReadInt32();
            tray.trayFadeInIntervalToSeconds = reader.ReadInt32();
            tray.trayFadeOutIntervalFromSeconds = reader.ReadInt32();
            tray.trayFadeOutIntervalToSeconds = reader.ReadInt32();

            return tray;
        }

    }
}


/*

"<Filename:"
"<Text:"
"<UseCooldown:"
"<Cooldown:"
"<FadeIn:"
"<FadeOut:"
"<TextColor:R("
")G(" + button.
")B(" + button.
")>";



*/
