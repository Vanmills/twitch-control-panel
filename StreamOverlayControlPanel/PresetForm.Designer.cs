﻿namespace StreamOverlayControlPanel
{
    partial class PresetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addPresetButton = new System.Windows.Forms.Button();
            this.presetTickerTextLabel = new System.Windows.Forms.Label();
            this.tickerTextTextbox = new System.Windows.Forms.TextBox();
            this.presetTickerTextColorButton = new System.Windows.Forms.Button();
            this.presetTickerTextPicturebox = new System.Windows.Forms.PictureBox();
            this.presetTickerBackground = new System.Windows.Forms.Button();
            this.presetTickerBackgroundColorPicturebox = new System.Windows.Forms.PictureBox();
            this.presetBackgroundColorButton = new System.Windows.Forms.Button();
            this.presetSelectedBackgroundColorPicturebox = new System.Windows.Forms.PictureBox();
            this.presetSetNameLabel = new System.Windows.Forms.Label();
            this.presetSetNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.addImagesButton = new System.Windows.Forms.Button();
            this.imageListCombobox = new System.Windows.Forms.ComboBox();
            this.listOfImageLabel = new System.Windows.Forms.Label();
            this.backgroundColorPicker = new System.Windows.Forms.ColorDialog();
            this.tickerBackgroundColorPicker = new System.Windows.Forms.ColorDialog();
            this.tickerTextColorPicker = new System.Windows.Forms.ColorDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.traysListCombobox = new System.Windows.Forms.ComboBox();
            this.removeSelectedTrayButton = new System.Windows.Forms.Button();
            this.removeImageFromList = new System.Windows.Forms.Button();
            this.tickerBGOnOff = new System.Windows.Forms.CheckBox();
            this.tickerTextOnOff = new System.Windows.Forms.CheckBox();
            this.presetSpeedLabel = new System.Windows.Forms.Label();
            this.tickerRepeatTimeLabel = new System.Windows.Forms.Label();
            this.tickerRepeatTimeTextbox = new System.Windows.Forms.TextBox();
            this.tickerSpeedTextbox = new System.Windows.Forms.TextBox();
            this.addTraysButton = new IndexedButton();
            ((System.ComponentModel.ISupportInitialize)(this.presetTickerTextPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.presetTickerBackgroundColorPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.presetSelectedBackgroundColorPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // addPresetButton
            // 
            this.addPresetButton.Location = new System.Drawing.Point(247, 513);
            this.addPresetButton.Name = "addPresetButton";
            this.addPresetButton.Size = new System.Drawing.Size(113, 23);
            this.addPresetButton.TabIndex = 11;
            this.addPresetButton.Text = "Add Preset";
            this.addPresetButton.UseVisualStyleBackColor = true;
            this.addPresetButton.Click += new System.EventHandler(this.addPresetButton_Click);
            // 
            // presetTickerTextLabel
            // 
            this.presetTickerTextLabel.AutoSize = true;
            this.presetTickerTextLabel.Location = new System.Drawing.Point(15, 236);
            this.presetTickerTextLabel.Name = "presetTickerTextLabel";
            this.presetTickerTextLabel.Size = new System.Drawing.Size(61, 13);
            this.presetTickerTextLabel.TabIndex = 35;
            this.presetTickerTextLabel.Text = "Ticker Text";
            // 
            // tickerTextTextbox
            // 
            this.tickerTextTextbox.Location = new System.Drawing.Point(115, 233);
            this.tickerTextTextbox.Name = "tickerTextTextbox";
            this.tickerTextTextbox.Size = new System.Drawing.Size(248, 20);
            this.tickerTextTextbox.TabIndex = 6;
            // 
            // presetTickerTextColorButton
            // 
            this.presetTickerTextColorButton.Location = new System.Drawing.Point(15, 200);
            this.presetTickerTextColorButton.Name = "presetTickerTextColorButton";
            this.presetTickerTextColorButton.Size = new System.Drawing.Size(205, 24);
            this.presetTickerTextColorButton.TabIndex = 5;
            this.presetTickerTextColorButton.Text = "Ticker Text Color";
            this.presetTickerTextColorButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.presetTickerTextColorButton.UseVisualStyleBackColor = true;
            this.presetTickerTextColorButton.Click += new System.EventHandler(this.presetTickerTextColorButton_Click);
            // 
            // presetTickerTextPicturebox
            // 
            this.presetTickerTextPicturebox.BackColor = System.Drawing.Color.White;
            this.presetTickerTextPicturebox.Location = new System.Drawing.Point(251, 200);
            this.presetTickerTextPicturebox.Name = "presetTickerTextPicturebox";
            this.presetTickerTextPicturebox.Size = new System.Drawing.Size(112, 24);
            this.presetTickerTextPicturebox.TabIndex = 36;
            this.presetTickerTextPicturebox.TabStop = false;
            // 
            // presetTickerBackground
            // 
            this.presetTickerBackground.Location = new System.Drawing.Point(15, 170);
            this.presetTickerBackground.Name = "presetTickerBackground";
            this.presetTickerBackground.Size = new System.Drawing.Size(205, 24);
            this.presetTickerBackground.TabIndex = 4;
            this.presetTickerBackground.Text = "Ticker Background Color";
            this.presetTickerBackground.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.presetTickerBackground.UseVisualStyleBackColor = true;
            this.presetTickerBackground.Click += new System.EventHandler(this.presetTickerBackground_Click);
            // 
            // presetTickerBackgroundColorPicturebox
            // 
            this.presetTickerBackgroundColorPicturebox.BackColor = System.Drawing.Color.White;
            this.presetTickerBackgroundColorPicturebox.Location = new System.Drawing.Point(251, 170);
            this.presetTickerBackgroundColorPicturebox.Name = "presetTickerBackgroundColorPicturebox";
            this.presetTickerBackgroundColorPicturebox.Size = new System.Drawing.Size(112, 24);
            this.presetTickerBackgroundColorPicturebox.TabIndex = 38;
            this.presetTickerBackgroundColorPicturebox.TabStop = false;
            // 
            // presetBackgroundColorButton
            // 
            this.presetBackgroundColorButton.Location = new System.Drawing.Point(15, 140);
            this.presetBackgroundColorButton.Name = "presetBackgroundColorButton";
            this.presetBackgroundColorButton.Size = new System.Drawing.Size(205, 24);
            this.presetBackgroundColorButton.TabIndex = 3;
            this.presetBackgroundColorButton.Text = "Background Color";
            this.presetBackgroundColorButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.presetBackgroundColorButton.UseVisualStyleBackColor = true;
            this.presetBackgroundColorButton.Click += new System.EventHandler(this.presetBackgroundColorButton_Click);
            // 
            // presetSelectedBackgroundColorPicturebox
            // 
            this.presetSelectedBackgroundColorPicturebox.BackColor = System.Drawing.Color.White;
            this.presetSelectedBackgroundColorPicturebox.Location = new System.Drawing.Point(251, 140);
            this.presetSelectedBackgroundColorPicturebox.Name = "presetSelectedBackgroundColorPicturebox";
            this.presetSelectedBackgroundColorPicturebox.Size = new System.Drawing.Size(112, 24);
            this.presetSelectedBackgroundColorPicturebox.TabIndex = 40;
            this.presetSelectedBackgroundColorPicturebox.TabStop = false;
            // 
            // presetSetNameLabel
            // 
            this.presetSetNameLabel.AutoSize = true;
            this.presetSetNameLabel.Location = new System.Drawing.Point(12, 66);
            this.presetSetNameLabel.Name = "presetSetNameLabel";
            this.presetSetNameLabel.Size = new System.Drawing.Size(82, 13);
            this.presetSetNameLabel.TabIndex = 43;
            this.presetSetNameLabel.Text = "Name Of Preset";
            // 
            // presetSetNameTextBox
            // 
            this.presetSetNameTextBox.Location = new System.Drawing.Point(105, 63);
            this.presetSetNameTextBox.Name = "presetSetNameTextBox";
            this.presetSetNameTextBox.Size = new System.Drawing.Size(255, 20);
            this.presetSetNameTextBox.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "Create new Preset";
            // 
            // addImagesButton
            // 
            this.addImagesButton.Location = new System.Drawing.Point(18, 341);
            this.addImagesButton.Name = "addImagesButton";
            this.addImagesButton.Size = new System.Drawing.Size(110, 24);
            this.addImagesButton.TabIndex = 9;
            this.addImagesButton.Text = "Add Image";
            this.addImagesButton.UseVisualStyleBackColor = true;
            this.addImagesButton.Click += new System.EventHandler(this.addImagesButton_Click);
            // 
            // imageListCombobox
            // 
            this.imageListCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.imageListCombobox.FormattingEnabled = true;
            this.imageListCombobox.Location = new System.Drawing.Point(115, 307);
            this.imageListCombobox.Name = "imageListCombobox";
            this.imageListCombobox.Size = new System.Drawing.Size(248, 21);
            this.imageListCombobox.TabIndex = 47;
            this.imageListCombobox.TabStop = false;
            // 
            // listOfImageLabel
            // 
            this.listOfImageLabel.AutoSize = true;
            this.listOfImageLabel.Location = new System.Drawing.Point(15, 315);
            this.listOfImageLabel.Name = "listOfImageLabel";
            this.listOfImageLabel.Size = new System.Drawing.Size(72, 13);
            this.listOfImageLabel.TabIndex = 48;
            this.listOfImageLabel.Text = "List of Images";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 385);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "List Of Trays";
            // 
            // traysListCombobox
            // 
            this.traysListCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.traysListCombobox.FormattingEnabled = true;
            this.traysListCombobox.Location = new System.Drawing.Point(115, 385);
            this.traysListCombobox.Name = "traysListCombobox";
            this.traysListCombobox.Size = new System.Drawing.Size(248, 21);
            this.traysListCombobox.TabIndex = 50;
            this.traysListCombobox.TabStop = false;
            // 
            // removeSelectedTrayButton
            // 
            this.removeSelectedTrayButton.Location = new System.Drawing.Point(235, 416);
            this.removeSelectedTrayButton.Name = "removeSelectedTrayButton";
            this.removeSelectedTrayButton.Size = new System.Drawing.Size(128, 23);
            this.removeSelectedTrayButton.TabIndex = 12;
            this.removeSelectedTrayButton.TabStop = false;
            this.removeSelectedTrayButton.Text = "Remove Selected Tray";
            this.removeSelectedTrayButton.UseVisualStyleBackColor = true;
            this.removeSelectedTrayButton.Click += new System.EventHandler(this.removeSelectedTrayButton_Click);
            // 
            // removeImageFromList
            // 
            this.removeImageFromList.Location = new System.Drawing.Point(253, 341);
            this.removeImageFromList.Name = "removeImageFromList";
            this.removeImageFromList.Size = new System.Drawing.Size(110, 24);
            this.removeImageFromList.TabIndex = 11;
            this.removeImageFromList.TabStop = false;
            this.removeImageFromList.Text = "Remove Image";
            this.removeImageFromList.UseVisualStyleBackColor = true;
            this.removeImageFromList.Click += new System.EventHandler(this.removeImageFromList_Click);
            // 
            // tickerBGOnOff
            // 
            this.tickerBGOnOff.AutoSize = true;
            this.tickerBGOnOff.Checked = true;
            this.tickerBGOnOff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tickerBGOnOff.Location = new System.Drawing.Point(15, 102);
            this.tickerBGOnOff.Name = "tickerBGOnOff";
            this.tickerBGOnOff.Size = new System.Drawing.Size(151, 17);
            this.tickerBGOnOff.TabIndex = 1;
            this.tickerBGOnOff.Text = "Ticker Background On Off";
            this.tickerBGOnOff.UseVisualStyleBackColor = true;
            this.tickerBGOnOff.CheckedChanged += new System.EventHandler(this.tickerBGOnOff_CheckedChanged);
            // 
            // tickerTextOnOff
            // 
            this.tickerTextOnOff.AutoSize = true;
            this.tickerTextOnOff.Checked = true;
            this.tickerTextOnOff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tickerTextOnOff.Location = new System.Drawing.Point(247, 102);
            this.tickerTextOnOff.Name = "tickerTextOnOff";
            this.tickerTextOnOff.Size = new System.Drawing.Size(116, 17);
            this.tickerTextOnOff.TabIndex = 2;
            this.tickerTextOnOff.Text = "Ticker Text On/Off";
            this.tickerTextOnOff.UseVisualStyleBackColor = true;
            this.tickerTextOnOff.CheckedChanged += new System.EventHandler(this.tickerTextOnOff_CheckedChanged);
            // 
            // presetSpeedLabel
            // 
            this.presetSpeedLabel.AutoSize = true;
            this.presetSpeedLabel.Location = new System.Drawing.Point(15, 265);
            this.presetSpeedLabel.Name = "presetSpeedLabel";
            this.presetSpeedLabel.Size = new System.Drawing.Size(71, 13);
            this.presetSpeedLabel.TabIndex = 55;
            this.presetSpeedLabel.Text = "Ticker Speed";
            // 
            // tickerRepeatTimeLabel
            // 
            this.tickerRepeatTimeLabel.AutoSize = true;
            this.tickerRepeatTimeLabel.Location = new System.Drawing.Point(194, 265);
            this.tickerRepeatTimeLabel.Name = "tickerRepeatTimeLabel";
            this.tickerRepeatTimeLabel.Size = new System.Drawing.Size(113, 13);
            this.tickerRepeatTimeLabel.TabIndex = 56;
            this.tickerRepeatTimeLabel.Text = "Repeat Time Seconds";
            // 
            // tickerRepeatTimeTextbox
            // 
            this.tickerRepeatTimeTextbox.Location = new System.Drawing.Point(313, 262);
            this.tickerRepeatTimeTextbox.Name = "tickerRepeatTimeTextbox";
            this.tickerRepeatTimeTextbox.Size = new System.Drawing.Size(50, 20);
            this.tickerRepeatTimeTextbox.TabIndex = 8;
            this.tickerRepeatTimeTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // tickerSpeedTextbox
            // 
            this.tickerSpeedTextbox.Location = new System.Drawing.Point(115, 262);
            this.tickerSpeedTextbox.Name = "tickerSpeedTextbox";
            this.tickerSpeedTextbox.Size = new System.Drawing.Size(51, 20);
            this.tickerSpeedTextbox.TabIndex = 7;
            this.tickerSpeedTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validateData);
            // 
            // addTraysButton
            // 
            this.addTraysButton.ButtonID = -1;
            this.addTraysButton.Location = new System.Drawing.Point(18, 416);
            this.addTraysButton.Name = "addTraysButton";
            this.addTraysButton.Size = new System.Drawing.Size(128, 23);
            this.addTraysButton.TabIndex = 10;
            this.addTraysButton.Text = "Add Tray";
            this.addTraysButton.UseVisualStyleBackColor = true;
            this.addTraysButton.Click += new System.EventHandler(this.addTraysButton_Click);
            // 
            // PresetForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 565);
            this.Controls.Add(this.tickerSpeedTextbox);
            this.Controls.Add(this.tickerRepeatTimeTextbox);
            this.Controls.Add(this.tickerRepeatTimeLabel);
            this.Controls.Add(this.presetSpeedLabel);
            this.Controls.Add(this.tickerTextOnOff);
            this.Controls.Add(this.tickerBGOnOff);
            this.Controls.Add(this.removeImageFromList);
            this.Controls.Add(this.removeSelectedTrayButton);
            this.Controls.Add(this.traysListCombobox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addTraysButton);
            this.Controls.Add(this.listOfImageLabel);
            this.Controls.Add(this.imageListCombobox);
            this.Controls.Add(this.addImagesButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.presetSetNameLabel);
            this.Controls.Add(this.presetSetNameTextBox);
            this.Controls.Add(this.presetBackgroundColorButton);
            this.Controls.Add(this.presetSelectedBackgroundColorPicturebox);
            this.Controls.Add(this.presetTickerBackground);
            this.Controls.Add(this.presetTickerBackgroundColorPicturebox);
            this.Controls.Add(this.presetTickerTextColorButton);
            this.Controls.Add(this.presetTickerTextPicturebox);
            this.Controls.Add(this.presetTickerTextLabel);
            this.Controls.Add(this.tickerTextTextbox);
            this.Controls.Add(this.addPresetButton);
            this.Name = "PresetForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Streamoverlay - Add New Preset";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PresetForm_FormClosing);
            this.Load += new System.EventHandler(this.PresetForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.presetTickerTextPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.presetTickerBackgroundColorPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.presetSelectedBackgroundColorPicturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addPresetButton;
        private System.Windows.Forms.Label presetTickerTextLabel;
        private System.Windows.Forms.TextBox tickerTextTextbox;
        private System.Windows.Forms.Button presetTickerTextColorButton;
        private System.Windows.Forms.PictureBox presetTickerTextPicturebox;
        private System.Windows.Forms.Button presetTickerBackground;
        private System.Windows.Forms.PictureBox presetTickerBackgroundColorPicturebox;
        private System.Windows.Forms.Button presetBackgroundColorButton;
        private System.Windows.Forms.PictureBox presetSelectedBackgroundColorPicturebox;
        private System.Windows.Forms.Label presetSetNameLabel;
        private System.Windows.Forms.TextBox presetSetNameTextBox;
        private System.Windows.Forms.Label label4;
        private IndexedButton addTraysButton;
        private System.Windows.Forms.Button addImagesButton;
        private System.Windows.Forms.ComboBox imageListCombobox;
        private System.Windows.Forms.Label listOfImageLabel;
        private System.Windows.Forms.ColorDialog backgroundColorPicker;
        private System.Windows.Forms.ColorDialog tickerBackgroundColorPicker;
        private System.Windows.Forms.ColorDialog tickerTextColorPicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox traysListCombobox;
        private System.Windows.Forms.Button removeSelectedTrayButton;
        private System.Windows.Forms.Button removeImageFromList;
        private System.Windows.Forms.CheckBox tickerBGOnOff;
        private System.Windows.Forms.CheckBox tickerTextOnOff;
        private System.Windows.Forms.Label presetSpeedLabel;
        private System.Windows.Forms.Label tickerRepeatTimeLabel;
        private System.Windows.Forms.TextBox tickerRepeatTimeTextbox;
        private System.Windows.Forms.TextBox tickerSpeedTextbox;
    }
}