﻿using System;
using System.Linq;
using System.Text;

namespace StreamOverlayControlPanel
{
    public class PipePackage
    {
        public enum Packagetype
        {
            NotAValidPackage = 0,
            Background = 1,
            Layout = 2,
            Animation = 4,
            Information = 5,
            FadeOutTicker = 6,
            TickerColor = 7,
            TickerTextOnOff = 8,
            TickerBackgroundColor = 9,
            ImageFile = 10,
            ImageLocation = 11,
            WebcamChromaKey = 12,
            AddTray = 13,
            TrayAddedOk = 14,
            TrayOnOff = 15,
            TrayAutoAnimate = 16,
            Preset = 17,
            Quit = 255,
        }

        public Packagetype type;
        public int bufferSize;
        public byte[] buffer;
        public string clearText;

        public byte[] CreateBuffer(string pBuff, Packagetype pType)
        {
            buffer = new byte[pBuff.Length + sizeof(Packagetype) + sizeof(int)];
            byte[] header = new byte[sizeof(Packagetype)];
            header = BitConverter.GetBytes((int)pType).Take(sizeof(Packagetype)).ToArray();
            byte[] bufferLength = new byte[sizeof(int)];
            bufferLength = BitConverter.GetBytes(pBuff.Length).Take(sizeof (int)).ToArray();
            int i = 0;
            Array.Reverse(header);
            Array.Reverse(bufferLength);

            for (; i < header.Length; i++)
            {
                buffer[i] = header[i];
            }
            for (; i < sizeof(Packagetype) + sizeof(int); i++)
            {
                buffer[i] = bufferLength[i - sizeof(Packagetype)];
            }

            byte[] stringBuffer = Encoding.ASCII.GetBytes(pBuff);

            for (; i < buffer.Length; i++)
            {
                buffer[i] = stringBuffer[i - header.Length - bufferLength.Length];
            }
            
            return buffer;
        }

        public byte[] CreateBuffer(byte[] pBuff, Packagetype pType)
        {
            buffer = new byte[pBuff.Length + sizeof(Packagetype) + sizeof(int)];
            byte[] header = new byte[sizeof(Packagetype)];
            header = BitConverter.GetBytes((int)pType).Take(sizeof(Packagetype)).ToArray();
            byte[] bufferLength = new byte[sizeof(int)];
            bufferLength = BitConverter.GetBytes(pBuff.Length).Take(sizeof(int)).ToArray();
            Array.Reverse(header);
            Array.Reverse(bufferLength);

            int i = 0;
            for (; i < header.Length; i++)
            {
                buffer[i] = header[i];
            }
            for (int j = 0; j < bufferLength.Length; i++, j++)
            {
                buffer[i] = bufferLength[j];
            }

            for (int j = 0; j < pBuff.Length; i++, j++)
            {
                buffer[i] = pBuff[j];
            }
            return buffer;
        }

        public void DecodeBuffer(byte[] pBytesbuffer)
        {
            int i = 0;
            //int enumBufferTest = 0;
            byte[] enumArray = new byte[sizeof(Packagetype)];
            for (; i < sizeof(Packagetype); i++) // this will work for now until we have 255 options, think we can live with that.
            {
                enumArray[i] = pBytesbuffer[i];
                //enumBufferTest += pBytesbuffer[i];
            }
            Array.Reverse(enumArray);
            type = (Packagetype)BitConverter.ToInt32(enumArray, 0);

            int bufferLength = 0;
            byte[] bufferLengthArray = new byte[sizeof(int)];
            for (; i < sizeof(int) + sizeof(Packagetype); i++)
            {
                bufferLengthArray[i - sizeof(Packagetype)] = pBytesbuffer[i];
            }
            Array.Reverse(bufferLengthArray);
            bufferLength = BitConverter.ToInt32(bufferLengthArray, 0);
            bufferSize = bufferLength;

            buffer = new byte[bufferLength];
            for (int j = 0; j < bufferLength; i++)
            {
                buffer[j] = pBytesbuffer[i];
                j++;
            }
            clearText = Encoding.ASCII.GetString(buffer);
        }
    }
}
