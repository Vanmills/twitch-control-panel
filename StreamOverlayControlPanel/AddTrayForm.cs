﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StreamOverlayControlPanel
{
    public partial class AddTrayForm : Form
    {
        public Tray trayInformation;

        AddTrayForm()
        {

        }

        public AddTrayForm(Tray currentTray)
        {
            trayInformation = currentTray;
            InitializeComponent();
            if (string.IsNullOrEmpty(trayInformation.trayFilename))
            {
                HideControls();
            }
            else
            {
                trayFileNameLabelText.Text = Path.GetFileName(trayInformation.trayFilename);
                ShowControls();
            }
            for (int i = 1; i < 66; i++)
            {
                trayMinutesComboBox.Items.Add(i * 5);
            }
            //trayMinutesComboBox.Items.Add(75);
            //trayMinutesComboBox.Items.Add(90);
            //trayMinutesComboBox.Items.Add(105);
            //trayMinutesComboBox.Items.Add(120);
            //trayMinutesComboBox.Items.Add(150);
            //trayMinutesComboBox.Items.Add(180);
            //trayMinutesComboBox.Items.Add(240);
            InitializeControlValues();
        }

        private void InitializeControlValues()
        {
            
            traySelectedColorPictureBox.BackColor = trayInformation.trayTextColor;
            trayFadeInTextbox.Text = trayInformation.trayFadeInSpeed.ToString();
            trayFadeOutTextbox.Text = trayInformation.trayFadeOutSpeed.ToString();
            
            if (trayInformation.trayDirection == TrayDirection.Up)
            {
                trayAnimationDirectionCombobox.SelectedItem = "Up";
            }
            else if (trayInformation.trayDirection == TrayDirection.Down)
            {
                trayAnimationDirectionCombobox.SelectedItem = "Down";
            }
            else if (trayInformation.trayDirection == TrayDirection.Left)
            {
                trayAnimationDirectionCombobox.SelectedItem = "Left";
            }
            else // Right
            {
                trayAnimationDirectionCombobox.SelectedItem = "Right";
            }

            trayUseCountdownCheckbox.Checked = trayInformation.trayUseCountdown;
            trayUseTimerCheckbox.Checked = trayInformation.trayUseTimer;
            trayMinutesComboBox.SelectedItem = trayInformation.trayCountdownMinutes;
            trayAutoAnimateCheckbox.Checked = trayInformation.trayUseAutoAnimate;
            trayAutoAnimateIntervalFromTextbox.Text = trayInformation.trayFadeInIntervalFromSeconds.ToString();
            trayAutoAnimateIntervalToTextbox.Text = trayInformation.trayFadeInIntervalToSeconds.ToString();
            trayAutoAnimateFadeOutFromTextbox.Text = trayInformation.trayFadeOutIntervalFromSeconds.ToString();
            trayAutoAnimateFadeOutToTextbox.Text = trayInformation.trayFadeOutIntervalFromSeconds.ToString();


        }

        private void HideControls()
        {
            trayTextTextbox.Visible = false;
            trayTextLabel.Visible = false;
            trayTextColorLabel.Visible = false;
            traySelectColorButton.Visible = false;
            traySelectedColorPictureBox.Visible = false;
            traySelectedColorLabel.Visible = false;
            trayFadeInTextbox.Visible = false;
            trayFadeInSpeedLabel.Visible = false;
            trayFadeOutTextbox.Visible = false;
            trayFadeOutSpeedLabel.Visible = false;
            trayAnimationDirectionCombobox.Visible = false;
            trayAnimDirectionLabel.Visible = false;
            trayUseCountdownCheckbox.Visible = false;
            trayUseCountdownLabel.Visible = false;
            trayMinutesComboBox.Visible = false;
            trayCountdownMinutesLabel.Visible = false;
            trayUseTimerLabel.Visible = false;
            trayUseTimerCheckbox.Visible = false;
            trayAutoAnimateCheckbox.Visible = false;
            trayAutoAnimateLabel.Visible = false;
            trayAutoAnimateIntervalFromTextbox.Visible = false;
            trayAutoAnimateToLabel.Visible = false;
            trayRandomIntervalInfoLabel.Visible = false;
            trayAutoAnimateIntervalToTextbox.Visible = false;
            traySaveAndCloseButton.Visible = false;
            trayCloseWithoutSaveButton.Visible = true;
        }

        private void ShowControls()
        {
            trayTextTextbox.Visible = true;
            trayTextLabel.Visible = true;
            trayTextColorLabel.Visible = true;
            traySelectColorButton.Visible = true;
            traySelectedColorPictureBox.Visible = true;
            traySelectedColorLabel.Visible = true;
            trayFadeInTextbox.Visible = true;
            trayFadeInSpeedLabel.Visible = true;
            trayFadeOutTextbox.Visible = true;
            trayFadeOutSpeedLabel.Visible = true;
            trayAnimationDirectionCombobox.Visible = true;
            trayAnimDirectionLabel.Visible = true;
            trayUseCountdownCheckbox.Visible = true;
            trayUseCountdownLabel.Visible = true;
            trayUseTimerCheckbox.Visible = true;
            trayAutoAnimateCheckbox.Visible = true;
            trayAutoAnimateLabel.Visible = true;
            trayUseTimerLabel.Visible = true;

            if (trayInformation.trayUseCountdown)
            {
                trayUseCountdownCheckbox.Checked = true;
                trayCountdownMinutesLabel.Visible = true;
                trayMinutesComboBox.Visible = true;
                trayUseTimerCheckbox.Checked = false;
            } else if (trayInformation.trayUseTimer)
            {
                trayUseCountdownCheckbox.Checked = false;
                trayCountdownMinutesLabel.Visible = false;
                trayMinutesComboBox.Visible = false;
                trayUseTimerCheckbox.Checked = true;
            }
            else
            {
                trayUseCountdownCheckbox.Checked = false;
                trayCountdownMinutesLabel.Visible = false;
                trayMinutesComboBox.Visible = false;
                trayUseTimerCheckbox.Checked = false;
            }
            if (trayInformation.trayUseAutoAnimate)
            {
                trayAutoAnimateIntervalFromTextbox.Visible = true;
                trayAutoAnimateToLabel.Visible = true;
                trayAutoAnimateIntervalToTextbox.Visible = true;
                trayFadeoutToLabel.Visible = true;
                trayIntervalFadeOutLabel.Visible = true;
                trayAutoAnimateFadeOutToTextbox.Visible = true;
            }
            else
            {
                trayAutoAnimateIntervalFromTextbox.Visible = false;
                trayAutoAnimateToLabel.Visible = false;
                trayAutoAnimateIntervalToTextbox.Visible = false;
                trayFadeoutToLabel.Visible = false;
                trayIntervalFadeOutLabel.Visible = false;
                trayAutoAnimateFadeOutToTextbox.Visible = false;
            }
            traySaveAndCloseButton.Visible = true;
            trayCloseWithoutSaveButton.Visible = false;
        }


        private void onEnterCharacters(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != 8;
        }

        private void trayAddGraphics_Click(object sender, EventArgs e)
        {
            DialogResult result = trayGraphicsDialogBox.ShowDialog();
            if (result == DialogResult.OK)
            {
                ShowControls();
                trayFileNameLabelText.Text = Path.GetFileName(trayGraphicsDialogBox.FileName);
                trayInformation.trayFilename = trayGraphicsDialogBox.FileName;
            }
            //else
            //{
            //    HideControls();
            //    trayFileNameLabelText.Text = "";
            //    trayInformation.trayFilename = "";
            //}
        }

        private void traySelectColorButton_Click(object sender, EventArgs e)
        {
            DialogResult result = textColorDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                traySelectedColorPictureBox.BackColor = textColorDialog.Color;
            }
        }

        private void traySaveAndCloseButton_Click(object sender, EventArgs e)
        {
            trayInformation.trayText = trayTextTextbox.Text;
            trayInformation.trayTextColor = traySelectedColorPictureBox.BackColor;
            if (!string.IsNullOrEmpty(trayFadeInTextbox.Text))
            {
                trayInformation.trayFadeInSpeed = int.Parse(trayFadeInTextbox.Text);

            }
            else
            {
                trayInformation.trayFadeInSpeed = 20;
            }

            if (!string.IsNullOrEmpty(trayFadeOutTextbox.Text))
            {
                trayInformation.trayFadeOutSpeed = int.Parse(trayFadeOutTextbox.Text);
            }
            else
            {
                trayInformation.trayFadeOutSpeed = 10;
            }


            if (trayAnimationDirectionCombobox.SelectedItem.ToString() == "Up")
            {
                trayInformation.trayDirection = TrayDirection.Up;
            } else if (trayAnimationDirectionCombobox.SelectedItem.ToString() == "Down")
            {
                trayInformation.trayDirection = TrayDirection.Down;
            }
            else if (trayAnimationDirectionCombobox.SelectedItem.ToString() == "Left")
            {
                trayInformation.trayDirection = TrayDirection.Left;
            }
            else // Right
            {
                trayInformation.trayDirection = TrayDirection.Right;
            }
            if (trayUseCountdownCheckbox.Checked)
            {
                trayInformation.trayUseCountdown = true;
                trayInformation.trayCountdownMinutes = int.Parse(trayMinutesComboBox.SelectedItem.ToString());
                if (trayInformation.trayCountdownMinutes <= 0)
                {
                    trayInformation.trayUseCountdown = false;
                }
                trayInformation.trayUseTimer = false;
            } else if (trayUseTimerCheckbox.Checked)
            {
                trayInformation.trayUseCountdown = false;
                trayInformation.trayUseTimer = true;
            }
            trayInformation.trayUseAutoAnimate = trayAutoAnimateCheckbox.Checked;
            if (trayInformation.trayUseAutoAnimate)
            {
                if (!string.IsNullOrEmpty(trayAutoAnimateIntervalFromTextbox.Text))
                {
                    trayInformation.trayFadeInIntervalFromSeconds = int.Parse(trayAutoAnimateIntervalFromTextbox.Text);
                }
                else
                {
                    trayInformation.trayFadeInIntervalFromSeconds = 45;
                }
                if (!string.IsNullOrEmpty(trayAutoAnimateIntervalToTextbox.Text))
                {
                    trayInformation.trayFadeInIntervalToSeconds = int.Parse(trayAutoAnimateIntervalToTextbox.Text);
                }
                else
                {
                    trayInformation.trayFadeInIntervalToSeconds = 55;
                }


                if (trayInformation.trayFadeInIntervalFromSeconds <= 5)
                {
                    trayInformation.trayFadeInIntervalFromSeconds = Math.Abs(60 * trayInformation.trayFadeInIntervalFromSeconds);
                }
                if (trayInformation.trayFadeInIntervalToSeconds <= 5)
                {
                    trayInformation.trayFadeInIntervalToSeconds = Math.Abs(60 * trayInformation.trayFadeInIntervalToSeconds);
                }


                if (!string.IsNullOrEmpty(trayAutoAnimateFadeOutFromTextbox.Text))
                {
                    trayInformation.trayFadeOutIntervalFromSeconds = int.Parse(trayAutoAnimateFadeOutFromTextbox.Text);
                }
                else
                {
                    trayInformation.trayFadeOutIntervalFromSeconds = 45;
                }

                if (!string.IsNullOrEmpty(trayAutoAnimateFadeOutToTextbox.Text))
                {
                    trayInformation.trayFadeOutIntervalToSeconds = int.Parse(trayAutoAnimateFadeOutToTextbox.Text);
                }
                else
                {
                    trayInformation.trayFadeOutIntervalToSeconds = 55;
                }


                if (trayInformation.trayFadeOutIntervalFromSeconds <= 5)
                {
                    trayInformation.trayFadeOutIntervalFromSeconds = Math.Abs(60 * trayInformation.trayFadeOutIntervalFromSeconds);
                }
                if (trayInformation.trayFadeOutIntervalToSeconds <= 5)
                {
                    trayInformation.trayFadeOutIntervalToSeconds = Math.Abs(60 * trayInformation.trayFadeOutIntervalToSeconds);
                }

            }
            else
            {
                trayInformation.trayFadeInIntervalFromSeconds = 0;
                trayInformation.trayFadeInIntervalToSeconds = 0;
                trayInformation.trayFadeOutIntervalFromSeconds = 0;
                trayInformation.trayFadeOutIntervalToSeconds = 0;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void trayUseCountdownCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (trayUseCountdownCheckbox.Checked && trayUseTimerCheckbox.Checked)
            {
                trayUseTimerCheckbox.Checked = false;
            }
            trayCountdownMinutesLabel.Visible = trayUseCountdownCheckbox.Checked;
            trayMinutesComboBox.Visible = trayUseCountdownCheckbox.Checked;
        }

        private void trayUseTimerCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (trayUseTimerCheckbox.Checked && trayUseCountdownCheckbox.Checked)
            {
                trayUseCountdownCheckbox.Checked = false;
            }
            trayCountdownMinutesLabel.Visible = trayUseCountdownCheckbox.Checked;
            trayMinutesComboBox.Visible = trayUseCountdownCheckbox.Checked;

        }

        private void trayAutoAnimateCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            trayIntervalLabel.Visible = trayAutoAnimateCheckbox.Checked;
            trayAutoAnimateIntervalFromTextbox.Visible = trayAutoAnimateCheckbox.Checked;
            trayAutoAnimateToLabel.Visible = trayAutoAnimateCheckbox.Checked;
            trayAutoAnimateIntervalToTextbox.Visible = trayAutoAnimateCheckbox.Checked;
            trayRandomIntervalInfoLabel.Visible = trayAutoAnimateCheckbox.Checked;
            trayAutoAnimateFadeOutToTextbox.Visible = trayAutoAnimateCheckbox.Checked;
            trayIntervalFadeOutLabel.Visible = trayAutoAnimateCheckbox.Checked;
            trayFadeoutToLabel.Visible = trayAutoAnimateCheckbox.Checked;
            trayAutoAnimateFadeOutFromTextbox.Visible = trayAutoAnimateCheckbox.Checked;
        }

        private void trayCloseWithoutSaveButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
    
}
