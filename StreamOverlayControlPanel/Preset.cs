﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace StreamOverlayControlPanel
{
    [Serializable]
    public class UserColor
    {
        public byte R = 0;
        public byte G = 0;
        public byte B = 0;
        public byte A = 0;

        public static UserColor SetColor(Color color)
        {
            UserColor userColor = new UserColor();
            userColor.A = color.A;
            userColor.R = color.R;
            userColor.G = color.G;
            userColor.B = color.B;
            return userColor;
        }

        public void SetColor(byte r, byte g, byte b, byte a = 255)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public Color GetWindowsColor()
        {
            return Color.FromArgb(255, R, G, B);
        }
    }


    [Serializable]
    public class Preset
    {
        public string presetName = "";
        public bool resetPreset = false;
        public UserColor backgroundColor = new UserColor();
        public UserColor tickerBackgroundColor = new UserColor();
        public UserColor tickerTextColor = new UserColor();
        public string tickerText;
        public bool useTickerBackground = true;
        public bool useTickerText = true;
        public float tickerSpeed = 50.0f;
        public float tickerRepeatTime = 35.0f;

        public List<Tray> listOfTrays = new List<Tray>();
        public List<string> gameImages = new List<string>();

        public static byte[] SerializeClass(Preset pPreset)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);

            writer.Write(pPreset.presetName);
            writer.Write(pPreset.resetPreset);
            writer.Write(pPreset.backgroundColor.R);
            writer.Write(pPreset.backgroundColor.G);
            writer.Write(pPreset.backgroundColor.B);

            writer.Write(pPreset.tickerBackgroundColor.R);
            writer.Write(pPreset.tickerBackgroundColor.G);
            writer.Write(pPreset.tickerBackgroundColor.B);

            writer.Write(pPreset.tickerTextColor.R);
            writer.Write(pPreset.tickerTextColor.G);
            writer.Write(pPreset.tickerTextColor.B);

            writer.Write(pPreset.tickerText);

            writer.Write(pPreset.useTickerBackground);
            writer.Write(pPreset.useTickerText);
            writer.Write(pPreset.tickerSpeed);
            writer.Write(pPreset.tickerRepeatTime);

            writer.Write(pPreset.listOfTrays.Count);
            for (int i = 0; i < pPreset.listOfTrays.Count; i++)
            {
                writer.Write(pPreset.listOfTrays[i].trayID);
                writer.Write(pPreset.listOfTrays[i].trayFilename);
                writer.Write(pPreset.listOfTrays[i].trayText);
                writer.Write(pPreset.listOfTrays[i].trayTextColor.R);
                writer.Write(pPreset.listOfTrays[i].trayTextColor.G);
                writer.Write(pPreset.listOfTrays[i].trayTextColor.B);
                writer.Write(pPreset.listOfTrays[i].trayFadeInSpeed);
                writer.Write(pPreset.listOfTrays[i].trayFadeOutSpeed);
                writer.Write((int)pPreset.listOfTrays[i].trayDirection);
                writer.Write(pPreset.listOfTrays[i].trayUseCountdown);
                writer.Write(pPreset.listOfTrays[i].trayCountdownMinutes);
                writer.Write(pPreset.listOfTrays[i].trayUseTimer);
                writer.Write(pPreset.listOfTrays[i].trayUseAutoAnimate);
                writer.Write(pPreset.listOfTrays[i].trayFadeInIntervalFromSeconds);
                writer.Write(pPreset.listOfTrays[i].trayFadeInIntervalToSeconds);
                writer.Write(pPreset.listOfTrays[i].trayFadeOutIntervalFromSeconds);
                writer.Write(pPreset.listOfTrays[i].trayFadeOutIntervalToSeconds);
            }
            writer.Write(pPreset.gameImages.Count);
            for (int i = 0; i < pPreset.gameImages.Count; i++)
            {
                writer.Write(pPreset.gameImages[i]);
            }
            
            return ms.ToArray();
        }

        public static Preset DeserializeClass(byte[] buffer)
        {
            MemoryStream memStream = new MemoryStream(buffer);
            Preset preset = new Preset();
            BinaryReader reader = new BinaryReader(memStream);
            UserColor backgroundColor = new UserColor();
            UserColor tickerBackgroundColor = new UserColor();
            UserColor tickerTextColor = new UserColor();


            preset.presetName = reader.ReadString();
            preset.resetPreset = reader.ReadBoolean();
            backgroundColor.A = 255;
            backgroundColor.R = reader.ReadByte();
            backgroundColor.G = reader.ReadByte();
            backgroundColor.B = reader.ReadByte();

            preset.backgroundColor = backgroundColor;

            tickerBackgroundColor.A = 255;
            tickerBackgroundColor.R = reader.ReadByte();
            tickerBackgroundColor.G = reader.ReadByte();
            tickerBackgroundColor.B = reader.ReadByte();
            preset.tickerBackgroundColor = tickerBackgroundColor;

            tickerTextColor.A = 255;
            tickerTextColor.R = reader.ReadByte();
            tickerTextColor.G = reader.ReadByte();
            tickerTextColor.B = reader.ReadByte();
            preset.tickerTextColor = tickerTextColor;

            preset.tickerText = reader.ReadString();
            preset.useTickerBackground = reader.ReadBoolean();
            preset.useTickerText = reader.ReadBoolean();
            preset.tickerSpeed = reader.ReadSingle();
            preset.tickerRepeatTime = reader.ReadSingle();

            int trayCount = reader.ReadInt32();

            for (int i = 0; i < trayCount; i++)
            {
                Tray tray = new Tray();
                byte r;
                byte g;
                byte b;

                tray.trayID = reader.ReadInt32();
                tray.trayFilename = reader.ReadString();
                tray.trayText = reader.ReadString();

                r = reader.ReadByte();
                g = reader.ReadByte();
                b = reader.ReadByte();

                tray.trayTextColor = Color.FromArgb(255, r, g, b);
                tray.trayFadeInSpeed = reader.ReadInt32();
                tray.trayFadeOutSpeed = reader.ReadInt32();
                tray.trayDirection = (TrayDirection)reader.ReadInt32();
                tray.trayUseCountdown = reader.ReadBoolean();
                tray.trayCountdownMinutes = reader.ReadInt32();
                tray.trayUseTimer = reader.ReadBoolean();
                tray.trayUseAutoAnimate = reader.ReadBoolean();
                tray.trayFadeInIntervalFromSeconds = reader.ReadInt32();
                tray.trayFadeInIntervalToSeconds = reader.ReadInt32();
                tray.trayFadeOutIntervalFromSeconds = reader.ReadInt32();
                tray.trayFadeOutIntervalToSeconds = reader.ReadInt32();
                
                preset.listOfTrays.Add(tray);
            }
            int gameImagesCount = reader.ReadInt32();
            for (int i = 0; i < gameImagesCount; i++)
            {
                preset.gameImages.Add(reader.ReadString());
            }

            return preset;
        }

    }
}
