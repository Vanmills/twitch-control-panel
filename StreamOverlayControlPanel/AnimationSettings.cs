﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamOverlayControlPanel
{
    [Serializable]
    public class AnimationSettings
    {
        public int ID = -1;
        public string filenameGraphic = "";
        public float totalDuration = 10.0f; // total accessible timeline

        // Fade In
        public bool fadeIn = true;
        public float fadeInStartTime = 0.0f; // when should the fadein start in the timeline
        public float fadeInDuration = 0.5f; // how long is the fading animation going to last?

        // Fade Out
        public bool fadeOut = true;
        public float fadeOutStartTime = 9.0f; // When in the timeline should the fadeout start?
        public float fadeOutDuration = 1.0f; // how long should the fadeout last?

        // Rotate In
        public bool rotateIn = true; // Do you want the animation to rotate in?
        public bool rotateInClockwise = true; // Do you want the rotation to be clockwise or counter clockwise?
        public float rotateInStartTime = 0.1f; // When do you want the rotation to start in the timeline?
        public float rotateInDuration = 2.0f; // How long do you want the rotation to be?
        public float rotationInDegrees = 720.0f; // degrees to rotate during the duration of the in rotation;

        // Rotate Out
        public bool rotateOut = true; // Do you want the animation to rotate out?
        public bool rotateOutClockwise = true; // Do you want the out rotation animation to be clockwise or counter clockwise?
        public float rotateOutStartTime = 7.0f; // When in the timeline should the out rotation start?
        public float rotateOutDuration = 2.0f; // How long should the out rotation last?
        public float rotationOutDegrees = 720.0f; // How many degrees should the rotation be?

        // Zoom In
        public bool zoomIn = true; // Do you want to zoom the animation in?
        public float zoomInStartTime = 0.0f; // When do you want the start to occur in the timeline?
        public float zoomInSpeed = 100.0f; // How do you want the zoom In to last?
        public float zoomInSize = 135.0f;

        // Zoom out
        public bool zoomOut = true; // Do you want the animation to zoom out?
        public float zoomOutStartTime = 8.0f;
        public float zoomOutSpeed = 100.0f;
        public float zoomOutSize = 0.0f;

        // Zoom alternatives to be implemented later
        // Height / width zoom levels

        public static byte[] SerializeClass(AnimationSettings settings)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);
            writer.Write(settings.ID);
            writer.Write(settings.filenameGraphic);
            writer.Write(settings.totalDuration);
            writer.Write(settings.fadeIn);
            writer.Write(settings.fadeInStartTime);
            writer.Write(settings.fadeInDuration);
            writer.Write(settings.fadeOut);
            writer.Write(settings.fadeOutStartTime);
            writer.Write(settings.fadeOutDuration);
            writer.Write(settings.rotateIn);
            writer.Write(settings.rotateInClockwise);
            writer.Write(settings.rotateInStartTime);
            writer.Write(settings.rotateInDuration);
            writer.Write(settings.rotationInDegrees);
            writer.Write(settings.rotateOut);
            writer.Write(settings.rotateOutClockwise);
            writer.Write(settings.rotateOutStartTime);
            writer.Write(settings.rotateOutDuration);
            writer.Write(settings.rotationOutDegrees);
            writer.Write(settings.zoomIn);
            writer.Write(settings.zoomInStartTime);
            writer.Write(settings.zoomInSpeed);
            writer.Write(settings.zoomInSize);
            writer.Write(settings.zoomOut);
            writer.Write(settings.zoomOutStartTime);
            writer.Write(settings.zoomOutSpeed);
            writer.Write(settings.zoomOutSize);
            return ms.ToArray();
        }

        public static AnimationSettings DeserializeClass(byte[] buffer)
        {
            MemoryStream memStream = new MemoryStream(buffer);
            //byte[] arr = buffer;
            AnimationSettings spinZoom = new AnimationSettings();
            BinaryReader reader = new BinaryReader(memStream);
            spinZoom.ID = reader.ReadInt32();
            spinZoom.filenameGraphic = reader.ReadString();
            spinZoom.totalDuration = reader.ReadSingle();
            spinZoom.fadeIn = reader.ReadBoolean();
            spinZoom.fadeInStartTime = reader.ReadSingle();
            spinZoom.fadeInDuration = reader.ReadSingle();
            spinZoom.fadeOut = reader.ReadBoolean();
            spinZoom.fadeOutStartTime = reader.ReadSingle();
            spinZoom.fadeOutDuration = reader.ReadSingle();
            spinZoom.rotateIn = reader.ReadBoolean();
            spinZoom.rotateInClockwise = reader.ReadBoolean();
            spinZoom.rotateInStartTime = reader.ReadSingle();
            spinZoom.rotateInDuration = reader.ReadSingle();
            spinZoom.rotationInDegrees = reader.ReadSingle();
            spinZoom.rotateOut = reader.ReadBoolean();
            spinZoom.rotateOutClockwise = reader.ReadBoolean();
            spinZoom.rotateOutStartTime = reader.ReadSingle();
            spinZoom.rotateOutDuration = reader.ReadSingle();
            spinZoom.rotationOutDegrees = reader.ReadSingle();
            spinZoom.zoomIn = reader.ReadBoolean();
            spinZoom.zoomInStartTime = reader.ReadSingle();
            spinZoom.zoomInSpeed = reader.ReadSingle();
            spinZoom.zoomInSize = reader.ReadSingle();
            spinZoom.zoomOut = reader.ReadBoolean();
            spinZoom.zoomOutStartTime = reader.ReadSingle();
            spinZoom.zoomOutSpeed = reader.ReadSingle();
            spinZoom.zoomOutSize = reader.ReadSingle();
            return spinZoom;
        }

    }

    

}
