﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Xml.Serialization;
using StreamOverlayControlPanel.Properties;

namespace StreamOverlayControlPanel
{
    public partial class StreamOverlayForm : Form
    {
        private UdpClient client;
        private int numTrays = 0;
        private string controlName = "";
        private List<Tray> listOfTrays = new List<Tray>();
        private List<AnimationSettings> animateImageList = new List<AnimationSettings>();

        private List<Preset> listOfPresets = new List<Preset>();
        private Preset resetScenePreset = new Preset();
        private Preset defaultPreset = new Preset();
        private string versionNumber = "0.53.1a";
        // Load Event 
        private void FormMain_Load(object sender, EventArgs e)
        {
            if (Settings.Default.WindowLocation != null)
            {
                this.Location = Settings.Default.WindowLocation;
            }
        }

        private void CreateDefaultPreset()
        {
            resetScenePreset = new Preset();
            resetScenePreset.presetName = "Reset Scene";
            resetScenePreset.backgroundColor.SetColor(49, 77, 121);
            resetScenePreset.tickerRepeatTime = 35.0f;
            resetScenePreset.tickerSpeed = 50.0f;
            resetScenePreset.tickerTextColor.SetColor(255, 255, 255);
            resetScenePreset.tickerBackgroundColor.SetColor(59, 101, 164);
            resetScenePreset.tickerText = "Set your own tickerText here";
            resetScenePreset.useTickerBackground = true;
            resetScenePreset.useTickerText = true;

            defaultPreset = new Preset();
            defaultPreset.presetName = "Default preset";
            defaultPreset.backgroundColor.SetColor(49, 77, 121);
            defaultPreset.tickerRepeatTime = 35.0f;
            defaultPreset.tickerSpeed = 50.0f;
            defaultPreset.tickerTextColor.SetColor(255, 255, 255);
            defaultPreset.tickerBackgroundColor.SetColor(59, 101, 164);
            defaultPreset.tickerText = "Set your own tickerText here";
            defaultPreset.useTickerBackground = true;
            defaultPreset.useTickerText = true;

        }


        public StreamOverlayForm()
        {
            InitializeComponent();
            client = new UdpClient();
            //serverEndpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 63000);

            client.Connect(IPAddress.Parse("127.0.0.1"), 63000);
            //PipePackage package = new PipePackage();
            if (Width <= 365)
            {
                expandButton.Text = ">";
            }
            else
            {
                expandButton.Text = "<";
            }
            LoadAnimations();
            tickerTextSpeed.Text = "50";
            tickerRepeatTimeSeconds.Text = "35";
            CreateDefaultPreset();
            LoadPresets();
            versionLabel.Text = "Version: " + versionNumber;
        }

        public void AddToLog(string log)
        {
            debugLogWindow.Text += log + "\n\r";

        }

        private void actorButton_Click(object sender, EventArgs e)
        {
            PipePackage package = new PipePackage();
            package.CreateBuffer("TickerOnOff", PipePackage.Packagetype.TickerTextOnOff);
            //client.Connect(IPAddress.Parse("127.0.0.1"), 63000);
            client.Send(package.buffer, package.buffer.Length);

        }

        private void actorButton2_Click(object sender, EventArgs e)
        {
            PipePackage package = new PipePackage();
            package.CreateBuffer("NotRelevantForFadeout", PipePackage.Packagetype.FadeOutTicker);
            client.Send(package.buffer, package.buffer.Length);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                byte[] color = new byte[4];
                color[0] = colorDialog1.Color.R;
                color[1] = colorDialog1.Color.G;
                color[2] = colorDialog1.Color.B;
                color[3] = colorDialog1.Color.A;
                if (color[3] == 255)
                {
                    color[3] = 0;
                }
                PipePackage package = new PipePackage();

                //if (BitConverter.IsLittleEndian)
                //{
                //    Array.Reverse(color);
                //}
                package.CreateBuffer(color, PipePackage.Packagetype.Background);

                int bytevalue = BitConverter.ToInt32(color, 0);
                debugLogWindow.Text = bytevalue.ToString();

                client.Send(package.buffer, package.buffer.Length);

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // deal with cleanup here
            //isRunning = false;
            Settings.Default.WindowLocation = this.Location;
            Settings.Default.Save();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Graphics (*.png; *.jpg) | *.jpg;*.png";
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                PipePackage package = new PipePackage();
                // Open Sesame is the "code" to get to the right place.
                package.CreateBuffer(fileDialog.FileName, PipePackage.Packagetype.ImageFile);
                client.Send(package.buffer, package.buffer.Length);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PipePackage package = new PipePackage();
            float tickerSpeed = float.Parse(tickerTextSpeed.Text);
            //tickerSpeed /= 100.0f;
            float tickerRepeatTime = float.Parse(tickerRepeatTimeSeconds.Text);
            string tickerText = setTickerTextTextbox.Text;
            
            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);
            
            writer.Write(tickerSpeed);
            writer.Write(tickerRepeatTime);
            writer.Write(tickerText);

            package.CreateBuffer(ms.ToArray(), PipePackage.Packagetype.Information);
            client.Send(package.buffer, package.buffer.Length);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ColorDialog tickerColor = new ColorDialog();
            DialogResult result = tickerColor.ShowDialog();
            if (result == DialogResult.OK)
            {
                byte[] color = new byte[4];
                color[0] = tickerColor.Color.R;
                color[1] = tickerColor.Color.G;
                color[2] = tickerColor.Color.B;
                color[3] = tickerColor.Color.A;
                if (color[3] == 255)
                {
                    color[3] = 0;
                }
                PipePackage package = new PipePackage();

                package.CreateBuffer(color, PipePackage.Packagetype.TickerColor);
                client.Send(package.buffer, package.buffer.Length);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Graphics (*.png; *.jpg) | *.jpg;*.png";
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                PipePackage package = new PipePackage();
                package.CreateBuffer(fileDialog.FileName, PipePackage.Packagetype.ImageLocation);
                client.Send(package.buffer, package.buffer.Length);
            }
        }

        private void tickerBackgroundColorChange_Click(object sender, EventArgs e)
        {
            ColorDialog tickerColor = new ColorDialog();
            DialogResult result = tickerColor.ShowDialog();
            if (result == DialogResult.OK)
            {
                byte[] color = new byte[4];
                color[0] = tickerColor.Color.R;
                color[1] = tickerColor.Color.G;
                color[2] = tickerColor.Color.B;
                color[3] = tickerColor.Color.A;
                if (color[3] == 255)
                {
                    color[3] = 0;
                }
                PipePackage package = new PipePackage();

                package.CreateBuffer(color, PipePackage.Packagetype.TickerBackgroundColor);
                client.Send(package.buffer, package.buffer.Length);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "This program is created by: \n\rInge.\n\rVandenmills on twich (not an active streamer)\n\rsjokomelk<at>gmail.com",
                "About StreamOverlay" + versionNumber, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void webCamChromaKeyButton_Click(object sender, EventArgs e)
        {
            ColorDialog webChromeKeyColorDialog = new ColorDialog();

            DialogResult result = webChromeKeyColorDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                byte[] color = new byte[4];
                color[0] = webChromeKeyColorDialog.Color.R;
                color[1] = webChromeKeyColorDialog.Color.G;
                color[2] = webChromeKeyColorDialog.Color.B;
                color[3] = webChromeKeyColorDialog.Color.A;
                if (color[3] == 255)
                {
                    color[3] = 0;
                }

                PipePackage package = new PipePackage();
                package.CreateBuffer(color, PipePackage.Packagetype.WebcamChromaKey);
                client.Send(package.buffer, package.buffer.Length);
            }

        }

        private void expandButton_Click(object sender, EventArgs e)
        {

            if (Width <= 365)
            {
                MaximumSize = new Size(660, Height);
                Width = 660;
                expandButton.Text = "<";
            }
            else
            {
                MaximumSize = new Size(360, Height);
                Width = 365;
                expandButton.Text = ">";
            }

        }

        private void unityControlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Unity commands:\n\rLeftMousebutton + Q = Scale down selected image\n\rLeftMousebutton + R = Scale up selected image\n\rLeftMousebutton + Z = Rotate selected image cw\n\rLeftMousebutton + C = Rotate selected image ccw\n\rLeftMousebutton + X = Snap rotation to lowest 15degrees\n\rLeftMousebutton + Del = delete image\n\rLeftMousebutton + +/ - = Order images\n\r\n\rP - Webcam - experimental",
                "Unity commands", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void addTrayGraphicsButton_Click(object sender, EventArgs e)
        {
            Tray addThisTray = new Tray();
            AddTrayForm  trayForm = new AddTrayForm(addThisTray);
            DialogResult result = trayForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                AddTray(addThisTray);
                SendTrayToServer(numTrays);
                numTrays++;
            }

        }

        private void AddTray(Tray addThisTray)
        {
            addThisTray.trayID = numTrays;
            listOfTrays.Add(addThisTray);
            int yDistance = 25;
            controlName = Path.GetFileNameWithoutExtension(addThisTray.trayFilename);
            Label tray = new Label();
            tray.AutoSize = true;
            tray.Name = Path.GetFileName(addThisTray.trayFilename) + "label" + numTrays;
            tray.Location = new System.Drawing.Point(5, yDistance + 9 + 23 * numTrays);
            tray.Size = new System.Drawing.Size(75, 13);
            tray.AutoSize = false;
            tray.Text = controlName + ":";
            colorTooltip.SetToolTip(tray, Path.GetFileName(addThisTray.trayFilename));

            IndexedButton trayAnimateButton = new IndexedButton();
            trayAnimateButton.ButtonID = numTrays;
            trayAnimateButton.Name = controlName + "OnOffbutton" + numTrays;
            trayAnimateButton.Location = new System.Drawing.Point(90, yDistance + 5 + 23 * numTrays);
            if (addThisTray.trayUseAutoAnimate)
            {
                trayAnimateButton.Text = "AutoAnimate";
                trayAnimateButton.Click += new System.EventHandler(onAutomateSet);
            }
            else
            {
                trayAnimateButton.Text = "Animate";
                trayAnimateButton.Click += new System.EventHandler(turnOnOffTray);
            }
            trayAnimateButton.Size = new System.Drawing.Size(75, 20);

            IndexedButton resendButton = new IndexedButton();
            resendButton.Name = controlName + "Add Tray" + numTrays;
            resendButton.Location = new System.Drawing.Point(165, yDistance + 5 + 23 * numTrays);
            resendButton.Text = "Resend";
            resendButton.Size = new System.Drawing.Size(75, 20);
            resendButton.Click += new System.EventHandler(addTray_Click);
            resendButton.ButtonID = numTrays;

            IndexedButton editTray = new IndexedButton();
            editTray.Name = controlName + "Add Tray" + numTrays;
            editTray.Location = new System.Drawing.Point(245, yDistance + 5 + 23 * numTrays);
            editTray.Text = "Edit";
            editTray.Size = new System.Drawing.Size(75, 20);
            editTray.Click += new System.EventHandler(onEditTrayClick);
            editTray.ButtonID = numTrays;

            trayPanel.Controls.Add(tray);
            trayPanel.Controls.Add(trayAnimateButton);
            trayPanel.Controls.Add(editTray);
            trayPanel.Controls.Add(resendButton);

        }

        private void onAutomateSet(object sender, EventArgs e)
        {
            PipePackage package = new PipePackage();
            package.CreateBuffer(((IndexedButton)sender).ButtonID.ToString(), PipePackage.Packagetype.TrayAutoAnimate);
            client.Send(package.buffer, package.buffer.Length);
        }

        private void onEditTrayClick(object sender, EventArgs eventArgs)
        {
            int buttonID = ((IndexedButton) sender).ButtonID;
            string trayIDName = Path.GetFileName(listOfTrays[buttonID].trayFilename) + "label" + buttonID;
            string trayButtonIDName = Path.GetFileNameWithoutExtension(listOfTrays[buttonID].trayFilename) + "OnOffbutton" + buttonID;
            bool autoAnimate = listOfTrays[buttonID].trayUseAutoAnimate;
            
            //Path.GetFileName(addThisTray.trayFilename) + "label" + numTrays;
            AddTrayForm trayForm = new AddTrayForm(listOfTrays[buttonID]);

            DialogResult result = trayForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                debugLogWindow.Text = "Updated entry";
                bool changeButtonText = false;
                if (autoAnimate != listOfTrays[buttonID].trayUseAutoAnimate)
                {
                    changeButtonText = true;
                }

                for (int i = 0; i < trayPanel.Controls.Count; i++)
                {
                    if (trayPanel.Controls[i].Name == trayIDName)
                    {
                        trayPanel.Controls[i].Name = Path.GetFileName(listOfTrays[buttonID].trayFilename) + "label" + buttonID;
                        trayPanel.Controls[i].Text = "ID: " + Path.GetFileName(listOfTrays[buttonID].trayFilename) + ":";
                    }
                    if (changeButtonText)
                    {
                        if (trayPanel.Controls[i].Name == trayButtonIDName)
                        {
                            if (listOfTrays[buttonID].trayUseAutoAnimate)
                            {
                                trayPanel.Controls[i].Text = "AutoAnimate";
                                trayPanel.Controls[i].Click -= new System.EventHandler(turnOnOffTray);
                                trayPanel.Controls[i].Click += new System.EventHandler(onAutomateSet);
                            }
                            else
                            {
                                trayPanel.Controls[i].Text = "Animate";
                                trayPanel.Controls[i].Click += new System.EventHandler(turnOnOffTray);
                                trayPanel.Controls[i].Click -= new System.EventHandler(onAutomateSet);

                            }
                        }
                    }
                }
            }
        }

        private void SendTrayToServer(int pID)
        {
            PipePackage packet = new PipePackage();
            byte[] dataToSend = Tray.SerializeClass(listOfTrays[pID]);

            debugLogWindow.Text = listOfTrays[pID].trayFilename;
            packet.CreateBuffer(dataToSend, PipePackage.Packagetype.AddTray);
            client.Send(packet.buffer, packet.buffer.Length);
        }

        private void addTray_Click(object sender, EventArgs e)
        {
            // TODO: Rewrite
            PipePackage packet = new PipePackage();
            IndexedButton button = (IndexedButton) sender;
            byte[] dataToSend = Tray.SerializeClass(listOfTrays[button.ButtonID]);

            debugLogWindow.Text = listOfTrays[button.ButtonID].trayFilename;
            packet.CreateBuffer(dataToSend, PipePackage.Packagetype.AddTray);
            client.Send(packet.buffer, packet.buffer.Length);
        }

        private void turnOnOffTray(object sender, EventArgs e)
        {
            PipePackage packet = new PipePackage();
            IndexedButton button = (IndexedButton)sender;

            packet.CreateBuffer("<ID:" + button.ButtonID + ">", PipePackage.Packagetype.TrayOnOff);
            client.Send(packet.buffer, packet.buffer.Length);

        }

        private void addAnimatedImageButton_Click(object sender, EventArgs e)
        {
            AnimationSettings spzPackage = new AnimationSettings();
            AnimationForm animForm = new AnimationForm(spzPackage, client);
            DialogResult result = animForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                spzPackage.ID = animateImageList.Count;
                animateImageList.Add(spzPackage);
                animatedImagesCombobox.Items.Add(Path.GetFileNameWithoutExtension(spzPackage.filenameGraphic));
                animatedImagesCombobox.SelectedItem = Path.GetFileNameWithoutExtension(spzPackage.filenameGraphic);
                debugLogWindow.Text = "Animation added successfully";
                SaveAnimations();
            }
            else
            {
                debugLogWindow.Text = "Animation not saved. No graphic selected";
            }
        }

        private void SaveAnimations()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<AnimationSettings>));
            Stream stream = new FileStream(".\\animations.xml", FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, animateImageList);
            //for (int i = 0; i < animateImageList.Count; i++)
            //{
            //}
            stream.Flush();
            stream.Close();
            debugLogWindow.Text = "Animation list successfully saved!";

        }

        private void playAnimationButton_Click(object sender, EventArgs e)
        {
            if (animatedImagesCombobox.SelectedIndex >= 0)
            {
                PipePackage package = new PipePackage();
                package.CreateBuffer(
                    AnimationSettings.SerializeClass(animateImageList[animatedImagesCombobox.SelectedIndex]),
                    PipePackage.Packagetype.Animation);
                client.Send(package.buffer, package.buffer.Length);
            }
        }

        private void addPresetButton_Click(object sender, EventArgs e)
        {
            Preset preset = new Preset();
            PresetForm presetForm = new PresetForm(preset);
            DialogResult result = presetForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                presetList.Items.Add(preset.presetName);
                listOfPresets.Add(preset);
                presetList.SelectedItem = preset.presetName;
                SavePresets();
            }
        }

        private void usePresetButton_Click(object sender, EventArgs e)
        {
            if (presetList.SelectedIndex > -1)
            {
                PipePackage package = new PipePackage();
                Preset preset = null;
                if (presetList.SelectedIndex == 0)
                {
                    preset = resetScenePreset;
                } else if (presetList.SelectedIndex == 1)
                {
                    preset = defaultPreset;
                }
                else
                {
                    for (int i = 0; i < listOfPresets.Count; i++)
                    {
                        if (listOfPresets[i].presetName == (string)presetList.SelectedItem)
                        {
                            preset = listOfPresets[i];
                        }
                    }
                }
                if (preset != null)
                {
                    //preset.tickerSpeed /= 100.0f;
                    package.CreateBuffer(Preset.SerializeClass(preset), PipePackage.Packagetype.Preset);
                    client.Send(package.buffer, package.buffer.Length);
                    LoadPresetIntoFormData(preset);
                }
            }
        }

        private void LoadPresetIntoFormData(Preset preset)
        {
            tickerTextSpeed.Text = preset.tickerSpeed.ToString();
            tickerRepeatTimeSeconds.Text = preset.tickerRepeatTime.ToString();
            setTickerTextTextbox.Text = preset.tickerText;
            listOfTrays.Clear();
            
            trayPanel.Controls.Clear();
            numTrays = 0;
            for (int i = 0; i < preset.listOfTrays.Count; i++)
            {
                AddTray(preset.listOfTrays[i]);
                numTrays++;
            }

        }
        
        private void saveAnimationListButton_Click(object sender, EventArgs e)
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<AnimationSettings>));
                Stream stream = new FileStream(".\\animations.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, animateImageList);
                //for (int i = 0; i < animateImageList.Count; i++)
                //{
                //}
                stream.Flush();
                stream.Close();
                debugLogWindow.Text = "Animation list successfully saved!";
            }
            catch (Exception error)
            {
                debugLogWindow.Text = error.Message;
            }
        }

        private void LoadAnimations()
        {
            if (File.Exists(".\\animations.xml"))
            {

                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof (List<AnimationSettings>));
                    Stream stream = new FileStream(".\\animations.xml", FileMode.Open, FileAccess.Read,
                        FileShare.ReadWrite);
                    animateImageList = (List<AnimationSettings>) formatter.Deserialize(stream);
                    for (int i = 0; i < animateImageList.Count; i++)
                    {
                        animatedImagesCombobox.Items.Add(
                            Path.GetFileNameWithoutExtension(animateImageList[i].filenameGraphic));
                        
                    }
                    if (animateImageList.Count > 0)
                    {
                        animatedImagesCombobox.SelectedIndex = 0;
                    }
                    stream.Close();

                }
                catch (IOException error)
                {
                    debugLogWindow.Text = error.Message + " \nanimations.xml file is corrupt.";
                }
            }
        }

        private void removeAnimationButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(animatedImagesCombobox.SelectedItem.ToString()))
            {
                DialogResult result =
                    MessageBox.Show(
                        "Are you sure you want to remove the animation " + Path.GetFileNameWithoutExtension(animateImageList[animatedImagesCombobox.SelectedIndex].filenameGraphic) + "?", "Delete animation",
                        MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                    animateImageList.RemoveAt(animatedImagesCombobox.SelectedIndex);
                    animatedImagesCombobox.Items.RemoveAt(animatedImagesCombobox.SelectedIndex);
                    animatedImagesCombobox.SelectedIndex = 0;
                }
            }
        }

        private void validateData(object sender, KeyPressEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            //fileNameLabel.Text = textBox.Name;

            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar.Equals(',') && e.KeyChar.Equals('.');
            if (!char.IsControl(e.KeyChar) &&
                !char.IsDigit(e.KeyChar) &&
                e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            //check if '.' , ',' pressed
            char sepratorChar = ',';
            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
            if (e.KeyChar == ',')
            {
                // check if it's in the beginning of text not accept
                if (textBox.Text.Length == 0) e.Handled = true;
                // check if it's in the beginning of text not accept
                if (textBox.SelectionStart == 0) e.Handled = true;
                // check if there is already exist a '.' , ','
                if (alreadyExist(textBox.Text, ref sepratorChar)) e.Handled = true;
                //check if '.' or ',' is in middle of a number and after it is not a number greater than 99
                if (textBox.SelectionStart != textBox.Text.Length && e.Handled == false)
                {
                    // '.' or ',' is in the middle
                    string AfterDotString = textBox.Text.Substring(textBox.SelectionStart);

                    if (AfterDotString.Length > 2)
                    {
                        e.Handled = true;
                    }
                }
            }
            //check if a number pressed

            if (Char.IsDigit(e.KeyChar))
            {
                //check if a coma or dot exist
                if (alreadyExist(textBox.Text, ref sepratorChar))
                {
                    int sepratorPosition = textBox.Text.IndexOf(sepratorChar);
                    string afterSepratorString = textBox.Text.Substring(sepratorPosition + 1);
                    if (textBox.SelectionStart > sepratorPosition && afterSepratorString.Length > 1)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private bool alreadyExist(string _text, ref char KeyChar)
        {
            if (_text.IndexOf('.') > -1)
            {
                KeyChar = '.';
                return true;
            }
            if (_text.IndexOf(',') > -1)
            {
                KeyChar = ',';
                return true;
            }
            return false;
        }

        private void savePresetButton_Click(object sender, EventArgs e)
        {
            Stream stream = new FileStream(".\\controlpanel-preset.xml", FileMode.Create, FileAccess.Write, FileShare.None);
            XmlSerializer formatter = new XmlSerializer(typeof(List<Preset>));
            formatter.Serialize(stream, listOfPresets);
            stream.Flush();
            stream.Close();
        }

        private void SavePresets()
        {
            Stream stream = new FileStream(".\\controlpanel-preset.xml", FileMode.Create, FileAccess.Write, FileShare.None);
            XmlSerializer formatter = new XmlSerializer(typeof(List<Preset>));
            formatter.Serialize(stream, listOfPresets);
            stream.Flush();
            stream.Close();
        }


        private void LoadPresets()
        {
            if (File.Exists(".\\controlpanel-preset.xml"))
            {
                Stream stream = new FileStream(".\\controlpanel-preset.xml", FileMode.Open, FileAccess.Read,
                    FileShare.ReadWrite);
                XmlSerializer formatter = new XmlSerializer(typeof (List<Preset>));
                listOfPresets = (List<Preset>) formatter.Deserialize(stream);

                presetList.Items.Add(resetScenePreset.presetName);
                presetList.Items.Add(defaultPreset.presetName);
                for (int i = 0; i < listOfPresets.Count; i++)
                {
                    presetList.Items.Add(listOfPresets[i].presetName);
                }
                presetList.SelectedIndex = 1;
            }
            else
            {
                presetList.Items.Add(resetScenePreset.presetName);
                presetList.Items.Add(defaultPreset.presetName);
                presetList.SelectedIndex = 1;
            }
        }

        private void twitchTestButton_Click(object sender, EventArgs e)
        {
            var httpWebRequest =
                (HttpWebRequest) WebRequest.Create("https://api.twitch.tv/kraken/channels/sanctuary_of_wolves/follows");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/vnd.twitchtv.v3+json";
            httpWebRequest.Method = "GET";
            Stream objStream = httpWebRequest.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            //string responseFromServer = objReader.ReadToEnd();
            string response = objReader.ReadToEnd();
            int start = response.IndexOf("\"display_name\":\"") + "\"display_name\":\"".Length;
            if (start != -1)
            {
                int end = response.IndexOf("\"", start + 1);
                string name = response.Substring(start, end - start);
                debugLogWindow.Text = name;
            }
        }

        private void editPresetButton_Click(object sender, EventArgs e)
        {
            if (presetList.SelectedIndex >= 2)
            {
            Preset preset = null;
                int i = 0;
                for (;i < listOfPresets.Count; i++)
                {
                    if (listOfPresets[i].presetName == (string)presetList.SelectedItem)
                    {
                        preset = listOfPresets[i];
                        break;
                    }
                }
                PresetForm presetform = new PresetForm(preset, true);
                DialogResult result = presetform.ShowDialog();
                if (result == DialogResult.OK)
                {
                    listOfPresets[i] = preset;
                    SavePresets();
                }
            }
            
        }
    }
}


// Old code

//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.IO;
//using System.IO.Pipes;
//using System.Linq;
//using System.Security.Principal;
//using System.Text;
//using System.Threading;
//using System.Windows.Forms;

//namespace TwitchTest
//{
//    public partial class Form1 : Form
//    {

//        private NamedPipeClientStream namedPipeClient;
//        //private StreamWriter streamWriter;
//        //private bool pipeOpen = false;

//        ~Form1()
//        {
//            // namedPipeClient.Dispose();
//            namedPipeClient.Close();
//        }

//        public Form1()
//        {
//            InitializeComponent();
//            namedPipeClient = new NamedPipeClientStream(".", "TwitchO", PipeDirection.InOut);
//            namedPipeClient.Connect(600);
//            if (namedPipeClient.IsAsync)
//            {1
//                textBox1.Text = "Async";
//            }

//            string messageToServer = "Hello server! Happy to meet you!";
//            //byte[] message = Encoding.ASCII.GetBytes(messageToServer);
//            PipePackage test = new PipePackage();
//            test.type = PipePackage.Packagetype.Animation;
//            byte[] message = test.CreateBuffer(messageToServer, PipePackage.Packagetype.Information);

//            namedPipeClient.Write(message, 0, message.Length);
//            namedPipeClient.Flush();

//            //namedPipeClient.EndWrite(result);


//            //namedPipeClient.WriteAsync(message, 0, message.Length);

//            //try
//            //{
//            //    IAsyncResult endWriteResult = namedPipeClient.BeginWrite(message, 0, message.Length,
//            //        ar => textBox1.Text = "Done!", this);
//            //    namedPipeClient.EndWrite(endWriteResult);
//            //    if (result.IsCompleted)
//            //    {
//            //        namedPipeClient.FlushAsync();
//            //    }

//            //}
//            //catch (IOException e)
//            //{
//            //    textBox1.Text = e.Message;
//            //}


//        }

//        public void AddToLog(string log)
//        {
//            textBox1.Text += log + "\n\r";

//        }

//        //private void OnReceivedPackage(IAsyncResult ar)
//        //{
//        //    if (ar.IsCompleted)
//        //    {
//        //        textBox1.Text = "Quitting...";
//        //    }
//        //}


//        private void actorButton_Click(object sender, EventArgs e)
//        {
//            string messageToServer = "Hello server! Happy to meet you! Button 1 reporting!";
//            PipePackage package = new PipePackage();
//            byte[] message = package.CreateBuffer(messageToServer, PipePackage.Packagetype.Layout);

//            //byte[] message = Encoding.ASCII.GetBytes(messageToServer);
//            textBox1.Text = Encoding.ASCII.GetString(message);
//            namedPipeClient.Write(message, 0, message.Length);
//            namedPipeClient.Flush();


//        }

//        private void actorButton2_Click(object sender, EventArgs e)
//        {
//            string messageToServer = "Hello server! Happy to meet you! Button 2 reporting!";
//            PipePackage package = new PipePackage();
//            byte[] message = package.CreateBuffer(messageToServer, PipePackage.Packagetype.Animation);

//            //byte[] message = Encoding.ASCII.GetBytes(messageToServer);
//            //textBox1.Text = Convert.ToString(message);
//            namedPipeClient.Write(message, 0, message.Length);
//            namedPipeClient.Flush();
//            textBox1.Text = Encoding.ASCII.GetString(message);
//        }

//        private void button1_Click(object sender, EventArgs e)
//        {
//            DialogResult result = colorDialog1.ShowDialog();
//            if (result == DialogResult.OK)
//            {
//                byte[] color = new byte[4];
//                color[0] = colorDialog1.Color.R;
//                color[1] = colorDialog1.Color.G;
//                color[2] = colorDialog1.Color.B;
//                color[3] = colorDialog1.Color.A;
//                if (color[3] == 255)
//                {
//                    color[3] = 0;
//                }
//                PipePackage package = new PipePackage();
//                byte[] message = package.CreateBuffer(color, PipePackage.Packagetype.Background);
//                textBox1.Text = Encoding.ASCII.GetString(message);

//                namedPipeClient.Write(message, 0, message.Length);
//                namedPipeClient.Flush();
//                if (BitConverter.IsLittleEndian)
//                {
//                    Array.Reverse(color);
//                }
//                int bytevalue = BitConverter.ToInt32(color, 0);
//                //textBox1.Text = bytevalue.ToString();

//            }

//        }
//    }
//}


// old change image
//private void button1_Click_1(object sender, EventArgs e)
//{
//    OpenFileDialog fileDialog = new OpenFileDialog();
//    fileDialog.Filter = "Graphics (*.png; *.jpg) | *.jpg;*.png";
//    DialogResult result = fileDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        PipePackage package = new PipePackage();
//        string fileName = Path.GetFileName(fileDialog.FileName);
//        // Open Sesame is the "code" to get to the right place.
//        package.CreateBuffer("Open Sesame - Filename:" + fileName, PipePackage.Packagetype.ImageFile);
//        client.Send(package.buffer, package.buffer.Length);

//        IPEndPoint server = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 63002);
//        Socket thisClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
//        try
//        {

//            thisClient.Connect(server);
//            if (thisClient.Connected)
//            {
//                textBox1.Text = "Connected";
//                thisClient.SendFile(fileDialog.FileName);
//            }
//            thisClient.Shutdown(SocketShutdown.Both);
//        }
//        catch (SocketException error)
//        {
//            textBox1.Text = error.Message;
//        }
//        // open tcp for transfer
//    }
//}

//private void ListenForData()
//{
//    while (isRunning)
//    {
//        if (client.Available > 0)
//        {
//            byte[] receiveBuffer = client.Receive(ref serverEndpoint);

//            if (receiveBuffer != null && receiveBuffer.Length > 0)
//            {
//                textBox1.Text = "Connected";
//                //Debug.Log("Received data from client: " + clientEndpoint.ToString());
//                PipePackage incomingPacket = new PipePackage();
//                //Debug.Log("PipePackage created");
//                incomingPacket.DecodeBuffer(receiveBuffer);
//                if (incomingPacket.type == PipePackage.Packagetype.TrayAddedOk)
//                {

//                    //Label tray = new Label();
//                    //tray.AutoSize = true;
//                    //tray.Name = controlName + "label" + numTrays;
//                    //tray.Location = new System.Drawing.Point(5, 20*numTrays);
//                    //tray.Size = new System.Drawing.Size(60, 13);
//                    //tray.Text = "ID: " + numTrays.ToString() + ":";

//                    //TextBox trayTextbox = new TextBox();
//                    //trayTextbox.Location = new System.Drawing.Point(67, 13*numTrays);
//                    //trayTextbox.Name = controlName + "textBox" + numTrays;
//                    //trayTextbox.Size = new System.Drawing.Size(200, 20);
//                    //trayTextbox.Text = "";

//                    //IndexedButton trayButton = new IndexedButton();
//                    //trayButton.ButtonID = numTrays;
//                    //trayButton.Location = new System.Drawing.Point(270, 13*numTrays);
//                    //trayButton.Name = controlName + "button" + numTrays;
//                    //trayButton.Text = "Change tray text";
//                    //trayButton.UseVisualStyleBackColor = true;
//                    //trayButton.linkedTextbox = trayTextbox;
//                    //trayButton.Click += new System.EventHandler(this.addTrayText_Click);
//                    //trayPanel.Controls.Add(tray);
//                    //trayPanel.Controls.Add(trayTextbox);
//                    //trayPanel.Controls.Add(trayButton);
//                    //numTrays++;
//                    //isRunning = false;
//                }
//            }
//        }
//        Thread.Sleep(1000/30);
//    }

//}


/*
// Add Tray graphic click event:
 FileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Graphics (*.png; *.jpg) | *.jpg;*.png";
            DialogResult result = openFile.ShowDialog();
            byte[] receiveBuffer = null;
            if (result == DialogResult.OK)
            {
                controlName = Path.GetFileNameWithoutExtension(openFile.FileName);
                //readThread.Start();
                //client.Send(packet.buffer, packet.buffer.Length);
                int yDistance = 25;
                //isRunning = true;
                Label tray = new Label();
                tray.AutoSize = true;
                tray.Name = controlName + "label" + numTrays;
                tray.Location = new System.Drawing.Point(5, yDistance + 9 + 23 * numTrays);
                tray.Size = new System.Drawing.Size(60, 13);
                tray.Text = "ID: " + numTrays.ToString() + ":";
                colorTooltip.SetToolTip(tray, Path.GetFileName(openFile.FileName));

                TextBox trayTextbox = new TextBox();
                trayTextbox.Location = new System.Drawing.Point(40, yDistance + 5 + 23 * numTrays);
                trayTextbox.Name = controlName + "textBox" + numTrays;
                trayTextbox.Size = new System.Drawing.Size(200, 20);
                trayTextbox.Text = "";
                
                CheckBox trayCheckBox = new CheckBox();
                trayCheckBox.Location = new System.Drawing.Point(250, yDistance + 5 + 23 * numTrays);
                trayCheckBox.Name = controlName + "checkbox" + numTrays;
                trayCheckBox.Checked = false;
                trayCheckBox.Size = new Size(20, 20);

                ComboBox trayComboBox = new ComboBox();
                trayComboBox.DropDownStyle = ComboBoxStyle.DropDown;
                for (int i = 1; i < 61; i++)
                {
                    trayComboBox.Items.Add(i);
                }
                trayComboBox.KeyPress += new KeyPressEventHandler(onEnterCharacters);
                trayComboBox.Location = new System.Drawing.Point(280, yDistance + 5 + 23 * numTrays);
                //trayComboBox.AutoSize = false;
                trayComboBox.Size = new System.Drawing.Size(55, 20);
                
                TextBox trayFadeInSpeed = new TextBox();
                trayFadeInSpeed.Location = new Point(345, yDistance + 5 + 23 * numTrays);
                trayFadeInSpeed.KeyPress += new KeyPressEventHandler(onEnterCharacters);
                trayFadeInSpeed.Size = new Size(25, 20);
                trayFadeInSpeed.Text = "20";
                trayFadeInSpeed.Name = controlName + "trayFadeIn" + numTrays;
                
                TextBox trayFadeOutSpeed = new TextBox();
                trayFadeOutSpeed.Location = new Point(380, yDistance + 5 + 23 * numTrays);
                trayFadeOutSpeed.KeyPress += new KeyPressEventHandler(onEnterCharacters);
                trayFadeOutSpeed.Size = new Size(25, 20);
                trayFadeOutSpeed.Text = "20";
                trayFadeOutSpeed.Name = controlName + "trayFadeOut" + numTrays;

                PictureBox traySelectColorpPictureBox = new PictureBox();
                traySelectColorpPictureBox.BackColor = Color.White;
                traySelectColorpPictureBox.Name = controlName + "trayTextColor" + numTrays;
                traySelectColorpPictureBox.Click += new EventHandler(onTrayTextColorClick);
                traySelectColorpPictureBox.MouseHover += new EventHandler(SelectColorpPictureBoxOnMouseHover);
                colorTooltip.SetToolTip(traySelectColorpPictureBox, "Select Color");
                traySelectColorpPictureBox.Size = new Size(20, 20);
                traySelectColorpPictureBox.Location = new Point(430, yDistance + 5 + 23 * numTrays);
                
                IndexedButton trayButton = new IndexedButton();
                trayButton.ButtonID = numTrays;
                trayButton.Location = new System.Drawing.Point(543, yDistance + 5 + 23 * numTrays);
                trayButton.Name = controlName + "button" + numTrays;
                trayButton.Text = "Add tray text";
                trayButton.Size = new System.Drawing.Size(75, 20);
                trayButton.trayFilename = openFile.FileName;
                trayButton.UseVisualStyleBackColor = true;
                trayButton.trayTextTextbox = trayTextbox;
                trayButton.trayUseCooldownCheckbox = trayCheckBox;
                trayButton.trayCounterCombobox = trayComboBox;
                trayButton.trayFadeInTextbox = trayFadeInSpeed;
                trayButton.trayFadeOutTextbox = trayFadeOutSpeed;
                trayButton.trayColorBox = traySelectColorpPictureBox;

                IndexedButton trayOnOffButton = new IndexedButton();
                trayOnOffButton.ButtonID = numTrays;
                trayOnOffButton.Name = controlName + "OnOffbutton" + numTrays;
                trayOnOffButton.Location = new System.Drawing.Point(460, yDistance + 5 + 23 * numTrays);
                trayOnOffButton.Text = "Animate tray";
                trayOnOffButton.Size = new System.Drawing.Size(75, 20);
                trayOnOffButton.Click += new System.EventHandler(this.turnOnOffTray);

                trayButton.Click += new System.EventHandler(this.addTrayText_Click);
                trayPanel.Controls.Add(tray);
                trayPanel.Controls.Add(trayTextbox);
                trayPanel.Controls.Add(trayCheckBox);
                trayPanel.Controls.Add(trayComboBox);
                trayPanel.Controls.Add(trayFadeInSpeed);
                trayPanel.Controls.Add(trayFadeOutSpeed);
                trayPanel.Controls.Add(traySelectColorpPictureBox);
                trayPanel.Controls.Add(trayButton);
                trayPanel.Controls.Add(trayOnOffButton);
                numTrays++;
            }

         //private void onTrayTextColorClick(object sender, EventArgs eventArgs)
        //{
        //    DialogResult result = trayTextColorDialog.ShowDialog();
        //    if (result == DialogResult.OK)
        //    {
        //        PictureBox pictureBox = (PictureBox) sender;
        //        pictureBox.BackColor = trayTextColorDialog.Color;
        //    }
        //}

        //private void SelectColorpPictureBoxOnMouseHover(object sender, EventArgs eventArgs)
        //{
            

        //}

        //private void onEnterCharacters(object sender, KeyPressEventArgs e)
        //{
        //    e.Handled = !char.IsDigit(e.KeyChar);
        //}   
    
*/

//string dataToSend = "<ID:" + button.ButtonID + ">" +
//                    "<Filename:" + button.trayFilename + ">" +
//                    "<Text:" + button.trayTextTextbox.Text + ">" +
//                    "<UseCooldown:" + button.trayUseCooldownCheckbox.Checked + ">" +
//                    "<Cooldown:" + button.trayCounterCombobox.Text + ">" +
//                    "<FadeIn:" + button.trayFadeInTextbox.Text + ">" +
//                    "<FadeOut:" + button.trayFadeOutTextbox.Text + ">" +
//                    "<TextColor:R(" + button.trayColorBox.BackColor.R + 
//                    ")G(" + button.trayColorBox.BackColor.G + 
//                    ")B(" + button.trayColorBox.BackColor.B + 
//                    ")>";

//byte[] color = new byte[4];
//color[0] = webChromeKeyColorDialog.Color.R;
//color[1] = webChromeKeyColorDialog.Color.G;
//color[2] = webChromeKeyColorDialog.Color.B;
//color[3] = webChromeKeyColorDialog.Color.A;
//if (color[3] == 255)
//{
//    color[3] = 0;
//}


//"<TextColor:" + button.trayColorBox.BackColor.R + but;
//private void presetBackgroundColorButton_Click(object sender, EventArgs e)
//{
//    DialogResult result = presetColorDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        presetSelectedBackgroundColorPicturebox.BackColor = presetColorDialog.Color;
//    }
//}

//private void presetSelectedBackgroundColorPicturebox_Click(object sender, EventArgs e)
//{
//    DialogResult result = presetColorDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        presetSelectedBackgroundColorPicturebox.BackColor = presetColorDialog.Color;
//    }
//}

//private void presetTickerBackground_Click(object sender, EventArgs e)
//{
//    DialogResult result = presetTickerBackgroundColorDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        presetTickerBackgroundColorPicturebox.BackColor = presetTickerBackgroundColorDialog.Color;
//    }
//}

//private void presetTickerBackgroundColor_Click(object sender, EventArgs e)
//{
//    DialogResult result = presetTickerBackgroundColorDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        presetTickerBackgroundColorPicturebox.BackColor = presetTickerBackgroundColorDialog.Color;
//    }
//}

//private void presetTickerTextColorButton_Click(object sender, EventArgs e)
//{
//    DialogResult result = presetTickerTextColorDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        presetTickerTextPicturebox.BackColor = presetTickerTextColorDialog.Color;
//    }
//}

//private void presetTickerTextPicturebox_Click(object sender, EventArgs e)
//{
//    DialogResult result = presetTickerTextColorDialog.ShowDialog();
//    if (result == DialogResult.OK)
//    {
//        presetTickerTextPicturebox.BackColor = presetTickerTextColorDialog.Color;
//    }
//}